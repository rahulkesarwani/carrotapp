package com.carrotapp.di.components;


import android.app.Application;
import android.content.Context;

import com.carrotapp.MyApp;
import com.carrotapp.controller.IDataManager;
import com.carrotapp.di.annotations.ApplicationContext;
import com.carrotapp.di.modules.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ilkay on 26/02/2017.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(MyApp app);


    @ApplicationContext
    Context context();
    
    Application application();

    IDataManager getDataManager();

}

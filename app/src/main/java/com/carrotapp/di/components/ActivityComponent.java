/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.carrotapp.di.components;



import com.carrotapp.baseactivities.BaseActivity;
import com.carrotapp.di.annotations.PerActivity;
import com.carrotapp.di.modules.ActivityModule;
import com.carrotapp.ui.fragments.feedbackFragments.FeedbackFragment;
import com.carrotapp.ui.fragments.homeFragments.HomeFragment;
import com.carrotapp.ui.fragments.noHouseKeepingFragment.NoHouseKeepingFragment;
import com.carrotapp.ui.home.HomeActivity;
import com.carrotapp.ui.login.LoginActivity;
import com.carrotapp.ui.menus.MenuActivity;

import dagger.Component;

/**
 * Created by iaktas on 24/04/17.
 */


@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {


    void inject(BaseActivity baseActivity);

    void inject(LoginActivity activity);

    void inject(HomeActivity activity);

    void inject(MenuActivity activity);

    void inject(HomeFragment homeFragment);

    void inject(FeedbackFragment feedbackFragment);

    void inject(NoHouseKeepingFragment noHouseKeepingFragment);

}

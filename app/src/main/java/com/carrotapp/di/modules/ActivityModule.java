package com.carrotapp.di.modules;

import android.app.Activity;


import com.carrotapp.controller.IDataManager;
import com.carrotapp.di.annotations.PerActivity;
import com.carrotapp.ui.fragments.feedbackFragments.FeedbackFragmentMvpPresenter;
import com.carrotapp.ui.fragments.feedbackFragments.FeedbackFragmentMvpView;
import com.carrotapp.ui.fragments.feedbackFragments.FeedbackFragmentPresenter;
import com.carrotapp.ui.fragments.homeFragments.HomeFragmentMvpPresenter;
import com.carrotapp.ui.fragments.homeFragments.HomeFragmentMvpView;
import com.carrotapp.ui.fragments.homeFragments.HomeFragmentPresenter;
import com.carrotapp.ui.fragments.noHouseKeepingFragment.NoHouseKeepingFragmentMvpPresenter;
import com.carrotapp.ui.fragments.noHouseKeepingFragment.NoHouseKeepingFragmentMvpView;
import com.carrotapp.ui.fragments.noHouseKeepingFragment.NoHouseKeepingFragmentPresenter;
import com.carrotapp.ui.home.HomeMvpPresenter;
import com.carrotapp.ui.home.HomeMvpView;
import com.carrotapp.ui.home.HomePresenter;
import com.carrotapp.ui.login.LoginMvpPresenter;
import com.carrotapp.ui.login.LoginMvpView;
import com.carrotapp.ui.login.LoginPresenter;
import com.carrotapp.ui.menus.MenuMvpPresenter;
import com.carrotapp.ui.menus.MenuMvpView;
import com.carrotapp.ui.menus.MenuPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ilkay on 10/03/2017.
 */

@Module
public class ActivityModule {
    Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    Activity provideActivity() {
        return activity;
    }



    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> providesLoginPresenter(IDataManager IDataManager) {
        return new LoginPresenter<>(IDataManager);
    }


    @Provides
    @PerActivity
    HomeMvpPresenter<HomeMvpView> providesHomePresenter(IDataManager IDataManager) {
        return new HomePresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    MenuMvpPresenter<MenuMvpView> providesMenuPresenter(IDataManager IDataManager) {
        return new MenuPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    HomeFragmentMvpPresenter<HomeFragmentMvpView> providesHomeFragmentPresenter(IDataManager IDataManager) {
        return new HomeFragmentPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    FeedbackFragmentMvpPresenter<FeedbackFragmentMvpView> providesFeedbackFragmentPresenter(IDataManager IDataManager) {
        return new FeedbackFragmentPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    NoHouseKeepingFragmentMvpPresenter<NoHouseKeepingFragmentMvpView> providesNoHouseKeepingFragmentPresenter(IDataManager IDataManager) {
        return new NoHouseKeepingFragmentPresenter<>(IDataManager);
    }

 /*   @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> providesMainPresenter(IDataManager IDataManager) {
        return new MainPresenter<>(IDataManager);
    }*/

/*    @Provides
    @PerActivity
    AnotherMvpPresenter<AnotherMvpView> providesSlideUpPanelPresenter(IDataManager IDataManager) {
        return new AnotherPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    AppBarLayoutMvpPresenter<AppBarLayoutMvpView> providesAppBarLayoutPresenter(IDataManager IDataManager) {
        return new AppBarLayoutPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    AnotherFragmentMvpPresenter<AnotherFragmentMvpView> providesAnotherMvpPresenter(IDataManager IDataManager) {
        return new AnotherFragmentPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    RecyclerViewMvpPresenter<RecyclerViewMvpView> providesRecyclerViewMvpPresenter(IDataManager IDataManager) {
        return new RecyclerViewPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    BottomNavigationMvpPresenter<BottomNavigationMvpView> providesBottomNavigationMvpPresenter(IDataManager IDataManager) {
        return new BottomNavigationPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    AdsShowcaseMvpPresenter<AdsShowcaseMvpView> providesAdsShowcaseMvpPresenter(IDataManager IDataManager) {
        return new AdsShowcasePresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    AnimationMvpPresenter<AnimationMvpView> providesAnimationMvpPresenter(IDataManager IDataManager) {
        return new AnimationPresenter<>(IDataManager);
    }


    @Provides
    @PerActivity
    MobssAsyncTask providesMobssAsyncTask(Activity activity, Strategy strategy) {
        return new MobssAsyncTask(activity, strategy);
    }*/

}

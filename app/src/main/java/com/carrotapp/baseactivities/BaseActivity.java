package com.carrotapp.baseactivities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.carrotapp.BuildConfig;
import com.carrotapp.MyApp;
import com.carrotapp.R;
import com.carrotapp.baseactivities.base.MvpView;
import com.carrotapp.di.components.ActivityComponent;
//import com.globalproject.di.components.DaggerActivityComponent;
import com.carrotapp.di.components.DaggerActivityComponent;
import com.carrotapp.di.modules.ActivityModule;
import com.carrotapp.models.logins.LoginResponseModel;
import com.carrotapp.utils.AppConstants;
import com.carrotapp.utils.ConnectionDetector;
import com.carrotapp.utils.MySharedPreferences;


import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLKeyException;

import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;

/**
 * Created by rahul on 1/5/2018.
 */

public abstract class BaseActivity extends AppCompatActivity implements MvpView, BaseFragment.Callback, View.OnClickListener {


    public MySharedPreferences mySharedPreferences;
    public View[] errorViews;
    public ConnectionDetector mConnectionDetector;
    public String TAG = BaseActivity.class.getSimpleName();
    private ActivityComponent mActivityComponent;
    private Unbinder mUnBinder;

    private Pattern pattern;
    private Matcher matcher;
    private final String EMAIL_PATTERN = "[a-zA-z_.0-9]+@[a-zA-Z]+[.][a-zA-Z.]+";
    private final String MOBILE_PATTERN = "^[6-9][0-9]{9}$";
    private final String NAME_PATTERN = "^[a-zA-Z ]*$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getActivityLayout());

        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((MyApp) getApplication()).getAppComponent())
                .build();

        mActivityComponent.inject(this);

        mySharedPreferences = new MySharedPreferences(this);
        mConnectionDetector = new ConnectionDetector(this);
        errorViews = new View[9];
    }


    protected abstract int getActivityLayout();

    protected abstract void initUI();

    public ActivityComponent getActivityComponent() {
        return mActivityComponent;
    }


    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {

    }

    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    @Override
    protected void onDestroy() {

        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        super.onDestroy();
    }


    public void startActivity(Class<?> newActivityClass) {

        Intent intent = new Intent(getBaseContext(), newActivityClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intent);
    }


    protected void initErrorViews(View view) {
        errorViews[0] = view.findViewById(R.id.noInternetLayout);
        errorViews[1] = view.findViewById(R.id.slowInternetLayout);
        errorViews[2] = view.findViewById(R.id.serverErrorLayout);
        errorViews[3] = view.findViewById(R.id.noDataFoundLayout);
        errorViews[4] = view.findViewById(R.id.full_screen_progress_layout_white_complete);  /* full screen progress bar with background*/
        errorViews[5] = view.findViewById(R.id.full_screen_progress_layout);  /* full screen progress bar with trasnparent (No Background)*/
        errorViews[6] = view.findViewById(R.id.no_location_found);  /* No Location Found educate user to turn on location*/
        errorViews[7] = view.findViewById(R.id.noNotificationLayout);  /* No Location Found*/
        errorViews[8] = view;
        //findViewById(R.id.error_views_layout);
        errorViews[0].findViewById(R.id.try_again_button_internet).setOnClickListener(this);
        errorViews[1].findViewById(R.id.try_again_button_slownet).setOnClickListener(this);
        errorViews[2].findViewById(R.id.try_again_button_server).setOnClickListener(this);
        errorViews[3].findViewById(R.id.try_again_button_no_data).setOnClickListener(this);
        errorViews[6].findViewById(R.id.noLocationFoundButton).setOnClickListener(this);

        errorViews[8].setVisibility(View.VISIBLE);
    }

    public void showToast(String msg) {
        if (msg != null)
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }


    public void handleFailure(Exception exc) {
        errorViews[4].setVisibility(View.GONE);
        errorViews[5].setVisibility(View.GONE);
        try {
            throw new Exception(exc);
        } catch (UnknownHostException e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[1].setVisibility(View.VISIBLE);
        } catch (SSLHandshakeException e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[1].setVisibility(View.VISIBLE);
        } catch (SSLKeyException e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[1].setVisibility(View.VISIBLE);
        } catch (SSLException e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[1].setVisibility(View.VISIBLE);
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[2].setVisibility(View.VISIBLE);
        } catch (ConnectException e) {
            e.printStackTrace();
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[2].setVisibility(View.VISIBLE);
        } catch (IllegalStateException e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[2].setVisibility(View.VISIBLE);
            e.printStackTrace();
        } catch (Exception e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[1].setVisibility(View.VISIBLE);
            e.printStackTrace();
        } catch (Throwable e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[1].setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }


    public String getAccesstoken(MySharedPreferences mySharedPreferences) {
        String accessToken = mySharedPreferences.getString(AppConstants.ACCESS_TOKEN, "");
        return "Bearer " + accessToken;
    }


    protected boolean isResponseOK(short code) {
        errorViews[4].setVisibility(View.GONE);
        errorViews[5].setVisibility(View.GONE);
        boolean flag;
        switch (code) {
            case 404:
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[3].setVisibility(View.VISIBLE);
                flag = false;
                break;
            case 500:
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[2].setVisibility(View.VISIBLE);
                flag = false;
                break;
            case 502:
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[2].setVisibility(View.VISIBLE);
                flag = false;
                break;
            case 302:
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[2].setVisibility(View.VISIBLE);
                flag = false;
                break;
            case 401:
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[2].setVisibility(View.VISIBLE);
                /*startActivity(new Intent(this, LoginActivity.class));*/
                flag = false;
                break;
            case 400:
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[2].setVisibility(View.VISIBLE);
                flag = false;
                break;
            case 408:
//                client request time out
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[1].setVisibility(View.VISIBLE);
                flag = false;
                break;
            case 200:  //GET
                flag = true;
                break;
            case 201:  // POST
                flag = true;
                break;
            case 205: // DELETE
                flag = true;
                break;
            case 204:
                flag = false;
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[3].setVisibility(View.VISIBLE);
                break;

            default:
                flag = false;
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[2].setVisibility(View.VISIBLE);
                flag = false;
        }
        return flag;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.try_again_button_internet:
                errorViews[0].setVisibility(View.GONE);
                handleNoInternet();
                break;
            case R.id.try_again_button_slownet:
                errorViews[1].setVisibility(View.GONE);
                handleSlowInternet();
                break;
            case R.id.try_again_button_server:
                errorViews[2].setVisibility(View.GONE);
                handleServerError();
                break;
            case R.id.try_again_button_no_data:
                errorViews[3].setVisibility(View.GONE);
                handleNoDataFound();

        }
    }

    protected abstract void handleNoInternet();

    protected abstract void handleSlowInternet();

    protected abstract void handleServerError();

    protected abstract void handleNoDataFound();

    protected boolean checkBeforeApiHit() {

        if (mConnectionDetector.isInternetConnected()) {
            errorViews[4].setVisibility(View.VISIBLE);
            return true;
        } else {
            errorViews[0].setVisibility(View.VISIBLE);
            return false;
        }
    }

    protected boolean checkBeforeApiHit(boolean isFirstTime) {

        if (mConnectionDetector.isInternetConnected()) {
            if (isFirstTime)
                errorViews[4].setVisibility(View.VISIBLE);
            return true;
        } else {
            errorViews[0].setVisibility(View.VISIBLE);
            return false;
        }
    }

    protected boolean checkBeforeApiHit(int currentPage) {

        if (mConnectionDetector.isInternetConnected()) {
            if (currentPage == 1)
                errorViews[4].setVisibility(View.VISIBLE);
            return true;
        } else {
            errorViews[0].setVisibility(View.VISIBLE);
            return false;
        }
    }

    public void showDebugLog(String TAG, String msg) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, msg);
    }

    public void showErrorLog(String TAG, String msg) {
        if (BuildConfig.DEBUG)
            Log.e(TAG, msg);
    }


/*    private void showConfirmationAlertDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                getString(R.string.yes_txt),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        onYesClicked();
                    }
                });

        builder1.setNegativeButton(
                getString(R.string.no_txt),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onNoClicked();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    public void onYesClicked() {

    }

    protected void onNoClicked() {

    }

    protected void onOkClicked() {

    }

 */

    public void showAlertDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public boolean isUserLoggedIn() {
        boolean loginFlag = false;
        String mAuthString = "Bearer " + mySharedPreferences.getString(AppConstants.ACCESS_TOKEN, null);
//        mAuthString = null;
        if (!mAuthString.equalsIgnoreCase("Bearer null")) {
            loginFlag = true;
        } else {
            loginFlag = false;
        }
        return loginFlag;
    }

    public void errorToast(String errorMessage) {
        Toasty.error(this, errorMessage, Toast.LENGTH_SHORT);
    }


    public void infoToast(String errorMessage) {
        Toasty.info(this, errorMessage, Toast.LENGTH_SHORT);
    }

    protected String getRoomId() {
        LoginResponseModel responseModel = ((LoginResponseModel) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, LoginResponseModel.class));
        if (responseModel != null)
            return responseModel.getRoom().getId();
        else
            return "";
    }

    protected String getHotelId() {
        LoginResponseModel responseModel = ((LoginResponseModel) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, LoginResponseModel.class));
        if (responseModel != null)
            return responseModel.getHotel().getId();
        else
            return "";
    }

    protected String getCity() {
        LoginResponseModel responseModel = ((LoginResponseModel) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, LoginResponseModel.class));
        if (responseModel != null)
            return responseModel.getHotel().getLocation();
        else
            return "";
    }

    protected String getLatitude() {
        LoginResponseModel responseModel = ((LoginResponseModel) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, LoginResponseModel.class));
        if (responseModel != null)
            return responseModel.getHotel().getLatitude();
        else
            return "";
    }

    protected String getLocationKey() {
        LoginResponseModel responseModel = ((LoginResponseModel) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, LoginResponseModel.class));
        if (responseModel != null)
            return responseModel.getHotel().getLocationKey();
        else
            return "";
    }
    protected String getLongitude() {
        LoginResponseModel responseModel = ((LoginResponseModel) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, LoginResponseModel.class));
        if (responseModel != null)
            return responseModel.getHotel().getLongitude();
        else
            return "";
    }

    public boolean validateEmail(String hex) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(hex);
        return matcher.matches();

    }


    public boolean validateName(String hex) {
        pattern = Pattern.compile(NAME_PATTERN);
        matcher = pattern.matcher(hex);
        return matcher.matches();

    }

    public boolean validateMobile(String hex) {
        pattern = Pattern.compile(MOBILE_PATTERN);
        matcher = pattern.matcher(hex);
        return matcher.matches();

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }
}



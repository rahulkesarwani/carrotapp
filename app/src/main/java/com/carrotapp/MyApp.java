package com.carrotapp;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;

import com.carrotapp.controller.IDataManager;
import com.carrotapp.di.components.ApplicationComponent;
import com.carrotapp.di.modules.ApplicationModule;
import com.carrotapp.di.components.DaggerApplicationComponent;
import javax.inject.Inject;


public class MyApp extends Application {
	
	ApplicationComponent appComponent;

	@Inject
	IDataManager mIDataManager;
	
	@Override
	public void onCreate() {
		super.onCreate();

		initializeInjector();

		// Setup handler for uncaught exceptions.
		Thread.setDefaultUncaughtExceptionHandler (this::handleUncaughtException);
	}
	
	private void initializeInjector(){
		appComponent = DaggerApplicationComponent.builder()
				.applicationModule(new ApplicationModule(this))
				.build();
		
		appComponent.inject(this);
		
	}
	
	public ApplicationComponent getAppComponent(){
		return appComponent;
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

	public void handleUncaughtException (Thread thread, Throwable e)
	{
		Thread.UncaughtExceptionHandler uch = Thread.getDefaultUncaughtExceptionHandler();
		e.printStackTrace(); // not all Android versions will print the stack trace automatically

		System.out.println("UncaughtException is handled!");

		System.exit(-1);
	}
}
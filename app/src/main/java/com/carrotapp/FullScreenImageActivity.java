package com.carrotapp;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.palette.graphics.Palette;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.carrotapp.utils.AppConstants;
import com.github.chrisbanes.photoview.PhotoView;

public class FullScreenImageActivity extends AppCompatActivity {

    private String url;
    private PhotoView photoView;
    private ImageButton closeIB;
    private ProgressBar loading;
    private RelativeLayout parentRL;

    private String TAG = FullScreenImageActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getSharedElementEnterTransition().setDuration(300);
            getWindow().getSharedElementReturnTransition().setDuration(200).setInterpolator(new DecelerateInterpolator());
        }

        photoView = findViewById(R.id.image);
        closeIB = findViewById(R.id.ib_close);
        loading = findViewById(R.id.loading);
        parentRL = findViewById(R.id.rl_custom_layout);

        closeIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        supportPostponeEnterTransition();

        Bundle extras = getIntent().getExtras();

        String imageUrl = extras.getString(AppConstants.CHAT_PIC_URL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String imageTransitionName = extras.getString(AppConstants.EXTRA_ANIMAL_IMAGE_TRANSITION_NAME);
            photoView.setTransitionName(imageTransitionName);
        }

        Glide.with(this).asBitmap()
                .load(imageUrl)
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {

                        supportStartPostponedEnterTransition();
                        loading.setIndeterminate(false);
                        loading.setBackgroundColor(Color.LTGRAY);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        supportStartPostponedEnterTransition();
                        if (Build.VERSION.SDK_INT >= 16) {
                            parentRL.setBackground(new BitmapDrawable(getResources(), Constants.fastblur(Bitmap.createScaledBitmap(resource, 50, 50, true))));// ));
                        } else {
                            onPalette(Palette.from(resource).generate());

                        }
                        photoView.setImageBitmap(resource);

                        loading.setVisibility(View.GONE);
                        return false;
                    }
                })

                .into(photoView);
    }

    public void onPalette(Palette palette) {
        if (null != palette) {
            ViewGroup parent = (ViewGroup) photoView.getParent().getParent();
            parent.setBackgroundColor(palette.getDarkVibrantColor(Color.GRAY));
        }
    }
}

package com.carrotapp.ui.menus;

import com.carrotapp.baseactivities.base.BasePresenter;
import com.carrotapp.controller.IDataManager;
import com.carrotapp.models.logins.LoginRequestModel;
import com.carrotapp.models.menus.MenuRequestModel;
import com.carrotapp.ui.login.LoginMvpPresenter;
import com.carrotapp.ui.login.LoginMvpView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 *
 * */
public class MenuPresenter<V extends MenuMvpView> extends BasePresenter<V> implements MenuMvpPresenter<V> {


    public MenuPresenter(IDataManager IDataManager) {
        super(IDataManager);
    }

    @Override
    public void hitCategoryApi(MenuRequestModel requestModel) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitCategoryApi(requestModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccessCategory(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }

    @Override
    public void hitMenuListApi(MenuRequestModel requestModel) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitMenuListApi(requestModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccessMenuList(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }
}

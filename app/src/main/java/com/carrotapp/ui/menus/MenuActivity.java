package com.carrotapp.ui.menus;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.carrotapp.R;
import com.carrotapp.adapters.recyclerview.CategoryMenuAdapter;
import com.carrotapp.adapters.recyclerview.MenuAdapter;
import com.carrotapp.baseactivities.BaseActivity;
import com.carrotapp.models.CommonResponseModel;
import com.carrotapp.models.ParseResponseModel;
import com.carrotapp.models.menus.CategoryModel;
import com.carrotapp.models.menus.MenuRequestModel;
import com.carrotapp.models.menus.MenuResponseModel;
import com.carrotapp.utils.AppConstants;
import com.carrotapp.utils.CheckNullable;
import com.carrotapp.utils.EndlessRecyclerViewScrollListener;
import com.carrotapp.utils.Helper;
import com.carrotapp.utils.customviews.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class MenuActivity extends BaseActivity implements MenuMvpView, CategoryMenuAdapter.OnSelectedCategoryListener {


    @BindView(R.id.menuRV)
    RecyclerView menuRV;
    @BindView(R.id.backIV)
    RelativeLayout backIV;
    @BindView(R.id.topHeaderLL)
    LinearLayout topHeaderLL;
    @BindView(R.id.categoryRV)
    RecyclerView categoryRV;
    @BindView(R.id.callNumberTV)
    CustomTextView callNumberTV;
    @BindView(R.id.callLL)
    RelativeLayout callLL;


    private String TAG = MenuActivity.class.getSimpleName();


    @Inject
    MenuMvpPresenter<MenuMvpView> mPresenter;

    private MenuAdapter menuAdapter;
    private int hitApiCounter;
    private CategoryMenuAdapter categoryMenuAdapter;
    private List<CategoryModel> categorylList = new ArrayList<>();
    private List<MenuResponseModel> menuModelList = new ArrayList<>();
    private int selectedCategoryPosition = 0;
    private int currentPage = 1, totalPage;
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(MenuActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(MenuActivity.this);

        initUI();

    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_menu;
    }

    @Override
    protected void initUI() {

        callNumberTV.setText(CheckNullable.checkNullable(Helper.getInstance().getContactNo(getApplicationContext())));

        initErrorViews(findViewById(R.id.error_views_layout));
//        mList.add("zI-Pux4uaqM");
//        mList.add("zI-Pux4uaqM");
//        mList.add("zI-Pux4uaqM");
//        mList.add("zI-Pux4uaqM");
//        mList.add("zI-Pux4uaqM");
//        mList.add("zI-Pux4uaqM");
//        mList.add("zI-Pux4uaqM");
        menuAdapter = new MenuAdapter(this, menuModelList);
        StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        menuRV.setLayoutManager(mLayoutManager);
        menuRV.setAdapter(menuAdapter);

        scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Log.d(TAG, "onLoadMore: " + currentPage);
                if (currentPage < totalPage) {
                    currentPage += 1;
                    menuRV.post(new Runnable() {
                        @Override
                        public void run() {
                            menuModelList.add(null);
                            menuAdapter.notifyItemInserted(menuAdapter.getItemCount() - 1);
                            hitMenuListApiForCategory();
                        }
                    });
                }
            }
        };
        menuRV.addOnScrollListener(scrollListener);
        hitCategoryApi();
    }


    private void hitCategoryApi() {

        callLL.setVisibility(View.GONE);
        hitApiCounter = 1;
        if (checkBeforeApiHit()) {

            MenuRequestModel menuRequestModel = new MenuRequestModel();
            menuRequestModel.setAction("categories");
            menuRequestModel.setHotelId(AppConstants.HOTEL_ID);
            mPresenter.hitCategoryApi(menuRequestModel);

        }
    }

    private void hitMenuListApiForCategory() {

        hitApiCounter = 2;
        if (checkBeforeApiHit(currentPage)) {
            MenuRequestModel menuRequestModel = new MenuRequestModel();
            menuRequestModel.setAction("menu_for_category");
            menuRequestModel.setHotelId(AppConstants.HOTEL_ID);
            menuRequestModel.setPage(currentPage);
            menuRequestModel.setCategoryId(Integer.parseInt(categorylList.get(selectedCategoryPosition).getId()));
            mPresenter.hitMenuListApi(menuRequestModel);
        }
    }

    private void checkAndHitApi() {
        if (hitApiCounter == 1) {
            hitCategoryApi();
        } else if (hitApiCounter == 2) {
            hitMenuListApiForCategory();
        }
    }

    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {
        showDebugLog(TAG, "handleNoInternet");
        checkAndHitApi();
    }

    @Override
    protected void handleSlowInternet() {
        showDebugLog(TAG, "handleSlowInternet");
        checkAndHitApi();
    }

    @Override
    protected void handleServerError() {
        showDebugLog(TAG, "handleServerError");
        checkAndHitApi();
    }

    @Override
    protected void handleNoDataFound() {
        showDebugLog(TAG, "handleNoDataFound");
        checkAndHitApi();
    }


    @Override
    public void onSuccessCategory(Response<CommonResponseModel> response) {
        callLL.setVisibility(View.VISIBLE);
        if (isResponseOK((short) response.code())) {
            if (response.body().getSuccess()) {
                if (response.body().getData() == null)
                    return;

                categorylList = ParseResponseModel.getCategoryList(response.body().getData());

                categoryMenuAdapter = new CategoryMenuAdapter(this, categorylList, MenuActivity.this);
                categoryRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                categoryRV.setAdapter(categoryMenuAdapter);
                hitMenuListApiForCategory();
            } else {
                showAlertDialog(response.body().getMessage());
            }
        }
    }

    @Override
    public void onSuccessMenuList(Response<CommonResponseModel> response) {
        callLL.setVisibility(View.VISIBLE);
        if (currentPage == 1) {
            menuModelList.clear();
            menuAdapter.notifyDataSetChanged();
        } else {
            menuModelList.remove(menuModelList.size() - 1);
        }

        if (isResponseOK((short) response.code())) {
            if (response.body().getSuccess()) {
                if (response.body().getData() == null)
                    return;


                if (currentPage == 1) {


                }
                menuModelList.addAll(ParseResponseModel.getMenuList(response.body().getData()));
                totalPage = response.body().getTotalPage();
                menuAdapter.notifyDataSetChanged();
                scrollListener.setLoading();

            } else {
                showAlertDialog(response.body().getMessage());
            }
        }
    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @OnClick(R.id.backIV)
    public void onViewClicked() {
        finish();
    }

    @Override
    public void onCategorySelectedPosition(int position) {
       // callLL.setVisibility(View.GONE);
        if (position != selectedCategoryPosition) {
            this.selectedCategoryPosition = position;
            currentPage = 1;
            hitMenuListApiForCategory();
        }
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        Log.d(TAG, "onUserInteraction: ");
        resetDisconnectTimer();
    }


    private Handler disconnectHandler = new Handler();

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "run: User Not Interacting Now ");
            Intent intent=new Intent();
            setResult(RESULT_OK,intent);
            finish();
        }
    };

    public void resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, AppConstants.TIMER_OUT_USER_INTERACTION);
    }

    @Override
    protected void onPause() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

}

package com.carrotapp.ui.menus;

import com.carrotapp.baseactivities.base.MvpView;
import com.carrotapp.models.CommonResponseModel;

import retrofit2.Response;


public interface MenuMvpView extends MvpView {

    void onSuccessCategory(Response<CommonResponseModel> response);

    void onSuccessMenuList(Response<CommonResponseModel> response);

    void onFailure(Throwable error);

}

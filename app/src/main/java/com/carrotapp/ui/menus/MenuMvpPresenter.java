package com.carrotapp.ui.menus;

import com.carrotapp.baseactivities.base.MvpPresenter;
import com.carrotapp.models.logins.LoginRequestModel;
import com.carrotapp.models.menus.MenuRequestModel;
import com.carrotapp.ui.login.LoginMvpView;

/**
 * Created by rahul on 6/14/2018.
 */

public interface MenuMvpPresenter<V extends MenuMvpView>  extends MvpPresenter<V> {
    void hitCategoryApi(MenuRequestModel requestModel);

    void hitMenuListApi(MenuRequestModel requestModel);
}
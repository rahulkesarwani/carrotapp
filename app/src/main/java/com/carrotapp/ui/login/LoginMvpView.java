package com.carrotapp.ui.login;

import com.carrotapp.baseactivities.base.MvpView;
import com.carrotapp.models.CommonResponseModel;
import com.carrotapp.models.logins.LoginResponseModel;

import retrofit2.Response;


public interface LoginMvpView extends MvpView {

    void onSuccess(Response<CommonResponseModel> response);

    void onFailure(Throwable error);

}

package com.carrotapp.ui.login;

import com.carrotapp.baseactivities.base.MvpPresenter;
import com.carrotapp.models.logins.LoginRequestModel;
import com.carrotapp.ui.home.HomeMvpView;

import okhttp3.MultipartBody;

/**
 * Created by rahul on 6/14/2018.
 */

public interface LoginMvpPresenter<V extends LoginMvpView>  extends MvpPresenter<V> {
    void hitLogin(LoginRequestModel loginRequestModel);
}
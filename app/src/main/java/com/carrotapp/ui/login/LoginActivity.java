package com.carrotapp.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.ImageView;

import com.carrotapp.R;
import com.carrotapp.baseactivities.BaseActivity;
import com.carrotapp.models.CommonResponseModel;
import com.carrotapp.models.ParseResponseModel;
import com.carrotapp.models.logins.LoginRequestModel;
import com.carrotapp.models.logins.LoginResponseModel;
import com.carrotapp.ui.home.HomeActivity;
import com.carrotapp.utils.AppConstants;
import com.carrotapp.utils.CheckNullable;
import com.carrotapp.utils.TimeHelper;
import com.carrotapp.utils.customviews.CustomEditText;
import com.carrotapp.utils.customviews.CustomTextView;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Response;


public class LoginActivity extends BaseActivity implements LoginMvpView {

    @BindView(R.id.roomNoET)
    CustomEditText roomNoET;
    @BindView(R.id.passwordIV)
    ImageView passwordIV;
    @BindView(R.id.passwordET)
    CustomEditText passwordET;
    @BindView(R.id.togglePassIV)
    ImageView togglePassIV;
    @BindView(R.id.loginTV)
    CustomTextView loginTV;
    @BindView(R.id.dateTV)
    CustomTextView dateTV;
    @BindView(R.id.timeTV)
    CustomTextView timeTV;
    @BindView(R.id.ampmTV)
    CustomTextView ampmTV;
    private String TAG = LoginActivity.class.getSimpleName();

    @Inject
    LoginMvpPresenter<LoginMvpView> mPresenter;

    private Handler timerHandler = null;
    private Runnable timerRunnable = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(LoginActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(LoginActivity.this);

        initUI();

    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void initUI() {
        startClock();
        initErrorViews(findViewById(R.id.error_views_layout));
    }


    private void hitLoginApi() {

        if (checkBeforeApiHit()) {


            LoginRequestModel loginRequestModel = new LoginRequestModel();
            loginRequestModel.setPassword(passwordET.getText().toString().trim());
            loginRequestModel.setRoomNo(roomNoET.getText().toString().trim());
            loginRequestModel.setHotelId(AppConstants.HOTEL_ID);
            loginRequestModel.setAction(AppConstants.LOGIN_ACTION);

            mPresenter.hitLogin(loginRequestModel);

        }
    }


    @Override
    public void onDestroy() {
        if (timerHandler != null)
            timerHandler.removeCallbacks(timerRunnable);
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {
        showDebugLog(TAG, "handleNoInternet");
        hitLoginApi();
    }

    @Override
    protected void handleSlowInternet() {
        showDebugLog(TAG, "handleSlowInternet");
        hitLoginApi();
    }

    @Override
    protected void handleServerError() {
        showDebugLog(TAG, "handleServerError");
        hitLoginApi();
    }

    @Override
    protected void handleNoDataFound() {
        showDebugLog(TAG, "handleNoDataFound");
        hitLoginApi();
    }


    @Override
    public void onSuccess(Response<CommonResponseModel> response) {
        if (isResponseOK((short) response.code())) {
            if (response.body().getSuccess()) {

                LoginResponseModel responseModel = ParseResponseModel.getLoginDetail(response.body().getData());
                mySharedPreferences.setObj(AppConstants.LOGIN_DATA, responseModel);
                mySharedPreferences.putBoolean(AppConstants.IS_USER_LOGGED_IN, true);
                startActivity(new Intent(this, HomeActivity.class));
                finishAffinity();
                //animateRightToLeft();
            } else {
                showAlertDialog(response.body().getMessage());
            }
        }
    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @OnClick({R.id.togglePassIV, R.id.loginTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.togglePassIV:
                handlePasswordToggle();
                break;
            case R.id.loginTV:
//                startActivity(new Intent(this, HomeActivity.class));
//                finishAffinity();
                if (validateFields())
                    hitLoginApi();
                break;
        }
    }

    private boolean validateFields() {
        if (roomNoET.getText().toString().trim().length() == 0) {
            errorToast("Please Enter Room Number");
            return false;
        } else if (passwordET.getText().toString().trim().length() == 0) {
            errorToast("Please Enter Password");
            return false;
        } else {
            return true;
        }
    }


    private void handlePasswordToggle() {
        if (togglePassIV.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.show).getConstantState())) {
            togglePassIV.setImageDrawable(getResources().getDrawable(R.drawable.hide));
            passwordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());

        } else {
            togglePassIV.setImageDrawable(getResources().getDrawable(R.drawable.show));
            passwordET.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }

        passwordET.setSelection(passwordET.length());
    }


    private void startClock() {
        timerHandler = new Handler();
        timerRunnable = new Runnable() {

            @Override
            public void run() {

                dateTV.setText(CheckNullable.checkNullable(TimeHelper.getInstance().getCurrentDate()));
                timeTV.setText(CheckNullable.checkNullable(TimeHelper.getInstance().getCurrentTime()));
                ampmTV.setText(" "+CheckNullable.checkNullable(TimeHelper.getInstance().getCurrentAMPM()));
                timerHandler.postDelayed(this, 1000);
            }
        };
        timerHandler.postDelayed(timerRunnable, 0);
    }


}

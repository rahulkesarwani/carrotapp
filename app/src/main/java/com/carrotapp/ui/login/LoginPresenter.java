package com.carrotapp.ui.login;

import com.carrotapp.baseactivities.base.BasePresenter;
import com.carrotapp.controller.IDataManager;
import com.carrotapp.models.logins.LoginRequestModel;
import com.carrotapp.models.logins.LoginResponseModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;
import retrofit2.Response;

/**
 *
 * */
public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V> implements LoginMvpPresenter<V> {


    public LoginPresenter(IDataManager IDataManager) {
        super(IDataManager);
    }


    @Override
    public void hitLogin(LoginRequestModel loginRequestModel) {

        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitLoginApi(loginRequestModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }
}

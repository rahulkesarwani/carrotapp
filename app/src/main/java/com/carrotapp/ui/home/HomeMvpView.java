package com.carrotapp.ui.home;

import com.carrotapp.baseactivities.base.MvpView;
import com.carrotapp.models.weathers.current.CurrentWeatherModel;
import com.carrotapp.models.weathers.forecast.ForecastWeatherModel;

import java.util.List;

import retrofit2.Response;


public interface HomeMvpView extends MvpView {

    void onSuccess(Response<ForecastWeatherModel> response);

    void onCurrentSuccess(Response<List<CurrentWeatherModel>> response);

    void onFailure(Throwable error);

}

package com.carrotapp.ui.home;

import com.carrotapp.baseactivities.base.MvpPresenter;
import com.carrotapp.models.logins.LoginRequestModel;
import com.carrotapp.ui.login.LoginMvpView;

import java.util.HashMap;

/**
 * Created by rahul on 6/14/2018.
 */

public interface HomeMvpPresenter<V extends HomeMvpView>  extends MvpPresenter<V> {

    void hitWeatherCurrentApi(String url,HashMap<String,String> querymap);

    void hitForecastWeatherApi(String url,HashMap<String,String> querymap);
}
package com.carrotapp.ui.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.carrotapp.R;
import com.carrotapp.baseactivities.BaseActivity;
import com.carrotapp.models.weathers.current.CurrentWeatherModel;
import com.carrotapp.models.weathers.forecast.ForecastWeatherModel;
import com.carrotapp.ui.fragments.feedbackFragments.FeedbackFragment;
import com.carrotapp.ui.fragments.homeFragments.HomeFragment;
import com.carrotapp.ui.fragments.noHouseKeepingFragment.NoHouseKeepingFragment;
import com.carrotapp.ui.fragments.videoFragment.VideoFragment;
import com.carrotapp.ui.menus.MenuActivity;
import com.carrotapp.utils.AppConstants;
import com.carrotapp.utils.CheckNullable;
import com.carrotapp.utils.GlideUtils;
import com.carrotapp.utils.TimeHelper;
import com.carrotapp.utils.customviews.CustomTextView;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class HomeActivity extends BaseActivity implements HomeMvpView, FragmentManager.OnBackStackChangedListener {


    private final int REQUEST_CODE_MENU = 1001;
    @BindView(R.id.feedbackIV)
    ImageView feedbackIV;
    @BindView(R.id.feedbackRL)
    RelativeLayout feedbackRL;
    @BindView(R.id.benefitIV)
    ImageView benefitIV;
    @BindView(R.id.benefitsRL)
    RelativeLayout benefitsRL;
    @BindView(R.id.menuIV)
    ImageView menuIV;
    @BindView(R.id.menuRL)
    RelativeLayout menuRL;
    @BindView(R.id.topHeaderLL)
    LinearLayout topHeaderLL;
    @BindView(R.id.todayDateTV)
    CustomTextView todayDateTV;
    @BindView(R.id.todayTemperatureTV)
    CustomTextView todayTemperatureTV;
    @BindView(R.id.firstLL)
    LinearLayout firstLL;
    @BindView(R.id.todaywindTV)
    CustomTextView todaywindTV;
    @BindView(R.id.secondndDayTV)
    CustomTextView secondndDayTV;
    @BindView(R.id.secondTemperatureTV)
    CustomTextView secondTemperatureTV;
    @BindView(R.id.secondLL)
    LinearLayout secondLL;
    @BindView(R.id.secondwindTV)
    CustomTextView secondwindTV;
    @BindView(R.id.thirdDateTV)
    CustomTextView thirdDateTV;
    @BindView(R.id.thirdTemperatureTV)
    CustomTextView thirdTemperatureTV;
    @BindView(R.id.thirdLL)
    LinearLayout thirdLL;
    @BindView(R.id.thirdwindTV)
    CustomTextView thirdwindTV;
    @BindView(R.id.layoutFrame)
    FrameLayout layoutFrame;
    @BindView(R.id.dateTV)
    CustomTextView dateTV;
    @BindView(R.id.timeTV)
    CustomTextView timeTV;
    @BindView(R.id.ampmTV)
    CustomTextView ampmTV;
    @BindView(R.id.logoIV)
    ImageView logoIV;
    @BindView(R.id.weatherParentLL)
    LinearLayout weatherParentLL;
    @BindView(R.id.todayTempIV)
    ImageView todayTempIV;
    @BindView(R.id.secondTempIV)
    ImageView secondTempIV;
    @BindView(R.id.thirdTempIV)
    ImageView thirdTempIV;
    @BindView(R.id.layoutRL)
    LinearLayout layoutRL;
    @BindView(R.id.clockLayout)
    View clockLayout;
    @BindView(R.id.homeIV)
    ImageView homeIV;
    @BindView(R.id.homeRL)
    RelativeLayout homeRL;
    @BindView(R.id.todayMinTempTV)
    CustomTextView todayMinTempTV;
    @BindView(R.id.todayMaxTempTV)
    CustomTextView todayMaxTempTV;
    @BindView(R.id.minmaxLL)
    LinearLayout minmaxLL;
    @BindView(R.id.secondMinTempTV)
    CustomTextView secondMinTempTV;
    @BindView(R.id.secondMaxTempTV)
    CustomTextView secondMaxTempTV;
    @BindView(R.id.minmaxsecondLL)
    LinearLayout minmaxsecondLL;
    @BindView(R.id.thirdMinTempTV)
    CustomTextView thidMinTempTV;
    @BindView(R.id.thirdMaxTempTV)
    CustomTextView thirdMaxTempTV;
    @BindView(R.id.minmaxthirdLL)
    LinearLayout minmaxthirdLL;


    private Handler timerHandler = null;
    private Runnable timerRunnable = null;

    private Handler weatherHandler = null;
    private Runnable weatherRunnable = null;

    private String TAG = HomeActivity.class.getSimpleName();

    @Inject
    HomeMvpPresenter<HomeMvpView> mPresenter;
    FragmentManager fragmentManager;

    private int hitApicounter;

    final String API_KEY = "aCCqJQVsvR3ynSTLEEh4uPiPrIpiedD3";

    private boolean isFirstTimeWeatherHitting = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(HomeActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(HomeActivity.this);

        initUI();

    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_home;
    }

    @Override
    protected void initUI() {
        initErrorViews(findViewById(R.id.error_views_layout));
        startClock();
        //  openHomeFragment();
        handleButtonClicks(-1);
        clearBackStack();
        openHomeFragment();


        hitCurrentWeatherApi();
        startTimerToRefreshWeather();
    }

    private void openHomeFragment() {
        HomeFragment homeFragment = new HomeFragment();
        changeFragment(homeFragment, AppConstants.HOME_FRAGMENT_KEY);
    }


    private void hitCurrentWeatherApi() {

        hitApicounter = 1;
        if (checkBeforeApiHit(isFirstTimeWeatherHitting)) {

            String url = "http://dataservice.accuweather.com/currentconditions/v1/" + getLocationKey();
            HashMap<String, String> querymap = new HashMap<>();

            querymap.put("apikey", API_KEY);
            querymap.put("language", "en-us");
            querymap.put("details", "true");
            // querymap.put("metric", "true");
            mPresenter.hitWeatherCurrentApi(url, querymap);
        }
    }


    private void hitForecastWeatherApi() {

        hitApicounter = 2;
        if (checkBeforeApiHit(isFirstTimeWeatherHitting)) {

            String url = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/" + getLocationKey();
            HashMap<String, String> querymap = new HashMap<>();

            querymap.put("apikey", API_KEY);
            querymap.put("language", "en-us");
            querymap.put("details", "true");
            querymap.put("metric", "true");
            mPresenter.hitForecastWeatherApi(url, querymap);
        }
    }
    // http://dataservice.accuweather.com/currentconditions/v1/26216?apikey=86H4udqyJyPfPWglyacSq2oJIGCZUfss&details=true&language=en-us

    @Override
    public void onDestroy() {
        if (timerHandler != null)
            timerHandler.removeCallbacks(timerRunnable);
        if (weatherHandler != null)
            weatherHandler.removeCallbacks(weatherRunnable);
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        super.onResume();
        resetDisconnectTimer();
    }

    private void checkAndHitApi() {
        if (hitApicounter == 1) {
            hitCurrentWeatherApi();
        } else if (hitApicounter == 2) {
            hitForecastWeatherApi();
        }
    }

    @Override
    protected void handleNoInternet() {
        showDebugLog(TAG, "handleNoInternet");
        checkAndHitApi();
    }

    @Override
    protected void handleSlowInternet() {
        showDebugLog(TAG, "handleSlowInternet");
        checkAndHitApi();
    }

    @Override
    protected void handleServerError() {
        showDebugLog(TAG, "handleServerError");
        checkAndHitApi();
    }

    @Override
    protected void handleNoDataFound() {
        showDebugLog(TAG, "handleNoDataFound");
        checkAndHitApi();
    }


    @Override
    public void onCurrentSuccess(Response<List<CurrentWeatherModel>> response) {

        if (response.code() == 200) {

            isFirstTimeWeatherHitting = false;
            try {
                String todayDate = TimeHelper.getInstance().getTodayDate();
                todayDateTV.setText(todayDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
            todayTemperatureTV.setText(Math.round(CheckNullable.checkNullable(response.body().get(0).getTemperature().getMetric().getValue())) + " \u2103");
            changeIcon(todayTempIV, response.body().get(0).getWeatherIcon());


        }

        hitForecastWeatherApi();

    }


    private void changeIcon(ImageView imageView, int weatherIcon) {

        if (weatherIcon <= 9) {
            GlideUtils.loadImage(this, "https://developer.accuweather.com/sites/default/files/0" + weatherIcon + "-s.png", imageView);

        } else {
            GlideUtils.loadImage(this, "https://developer.accuweather.com/sites/default/files/" + weatherIcon + "-s.png", imageView);

        }
    }

    @Override
    public void onSuccess(Response<ForecastWeatherModel> response) {

        errorViews[4].setVisibility(View.GONE);
        if (response.code() == 200) {
            todayMinTempTV.setText("" +(int) Math.floor(CheckNullable.checkNullable(response.body().getDailyForecasts().get(0).getTemperature().getMinimum().getValue())) + " \u2103");
            todayMaxTempTV.setText("" + (int)Math.floor(CheckNullable.checkNullable(response.body().getDailyForecasts().get(0).getTemperature().getMaximum().getValue())) + " \u2103");

            if (TimeHelper.getInstance().isNight()) {
                todaywindTV.setText("Wind " + CheckNullable.checkNullable(response.body().getDailyForecasts().get(0).getNight().getWind().getSpeed().getValue()) + " km/hr");
            } else {
                todaywindTV.setText("Wind " + CheckNullable.checkNullable(response.body().getDailyForecasts().get(0).getDay().getWind().getSpeed().getValue()) + " km/hr");
            }

            Log.d(TAG, "onSuccess: " + TimeHelper.getInstance().isNight());

            /*  end of today */

            try {
                String secondDayDate = TimeHelper.getInstance().getTomorrowDate();
                secondndDayTV.setText(secondDayDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            if (TimeHelper.getInstance().isNight()) {
//                secondwindTV.setText("Wind " + String.valueOf(response.body().getDailyForecasts().get(1).getNight().getWind().getSpeed().getValue()) + " km/hr");
//                secondTemperatureTV.setText(CheckNullable.checkNullable(response.body().getDailyForecasts().get(1).getNight().getIconPhrase()));
//
//                changeIcon(secondTempIV, response.body().getDailyForecasts().get(1).getDay().getIcon());
//
//            } else {
                secondwindTV.setText("Wind " + String.valueOf(response.body().getDailyForecasts().get(1).getDay().getWind().getSpeed().getValue()) + " km/hr");
                secondTemperatureTV.setText(CheckNullable.checkNullable(response.body().getDailyForecasts().get(1).getDay().getIconPhrase()));

                changeIcon(secondTempIV, response.body().getDailyForecasts().get(1).getNight().getIcon());

//            }
            secondMinTempTV.setText("" +(int) Math.floor(CheckNullable.checkNullable(response.body().getDailyForecasts().get(1).getTemperature().getMinimum().getValue())) + " \u2103");
            secondMaxTempTV.setText("" +(int) Math.floor(CheckNullable.checkNullable(response.body().getDailyForecasts().get(1).getTemperature().getMaximum().getValue())) + " \u2103");

            /* end of second day*/

            try {
                String thirdDayDate = TimeHelper.getInstance().getDayAfterTomorrowDate();
                thirdDateTV.setText(thirdDayDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
//            if (TimeHelper.getInstance().isNight()) {
//                thirdwindTV.setText("Wind " + String.valueOf(response.body().getDailyForecasts().get(2).getNight().getWind().getSpeed().getValue()) + " km/hr");
//                thirdTemperatureTV.setText(CheckNullable.checkNullable(response.body().getDailyForecasts().get(2).getNight().getIconPhrase()));
//                changeIcon(thirdTempIV, response.body().getDailyForecasts().get(2).getDay().getIcon());
//
//            } else {
                thirdwindTV.setText("Wind " + String.valueOf(response.body().getDailyForecasts().get(2).getDay().getWind().getSpeed().getValue()) + " km/hr");
                thirdTemperatureTV.setText(CheckNullable.checkNullable(response.body().getDailyForecasts().get(2).getDay().getIconPhrase()));
                changeIcon(thirdTempIV, response.body().getDailyForecasts().get(2).getNight().getIcon());

         //   }
            thidMinTempTV.setText("" +(int) Math.floor(CheckNullable.checkNullable(response.body().getDailyForecasts().get(2).getTemperature().getMinimum().getValue())) + " \u2103");
            thirdMaxTempTV.setText("" +(int) Math.floor(CheckNullable.checkNullable(response.body().getDailyForecasts().get(2).getTemperature().getMaximum().getValue())) + " \u2103");
        }else{
            showToast("Weather couldn't be refreshed.");
        }
    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }


    @OnClick({R.id.feedbackRL, R.id.benefitsRL, R.id.menuRL, R.id.homeRL, R.id.logoIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.feedbackRL:
                FeedbackFragment feedbackFragment = new FeedbackFragment();
                changeFragment(feedbackFragment, AppConstants.FEEDBACK_FRAGMENT_KEY);

                handleButtonClicks(0);
                // topHeaderLL.setVisibility(View.GONE);
                break;
            case R.id.benefitsRL:
                NoHouseKeepingFragment noHouseKeepingFragment = new NoHouseKeepingFragment();
                changeFragment(noHouseKeepingFragment, AppConstants.NO_HOUSE_KEEPING);

                handleButtonClicks(1);
                break;
            case R.id.homeRL:

                clearBackStack();
                openHomeFragment();
                handleButtonClicks(-1);
                break;
            case R.id.menuRL:
                Intent menuIntent = new Intent(this, MenuActivity.class);
                startActivityForResult(menuIntent, REQUEST_CODE_MENU);

                break;
            case R.id.logoIV:
                clearBackStack();
                openHomeFragment();
                handleButtonClicks(-1);
                break;
        }
    }


    public void changeFragment(Fragment fragment, String tag) {
        try {
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.layoutFrame, fragment).addToBackStack(tag).commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }

    public void removeFragments(String tag) {
        fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public void removeRecentFragments(String tag) {
        fragmentManager.popBackStackImmediate(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onBackStackChanged() {
        int count = fragmentManager.getBackStackEntryCount();
        for (int i = count - 1; i >= 0; i--) {
            FragmentManager.BackStackEntry entry = fragmentManager.getBackStackEntryAt(i);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: ");
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_MENU) {
            // if user has not interacted on menu activity then close menu and navigate him to the home.
            onViewClicked(homeRL);
        }
    }


    @Override
    public void onBackPressed() {
        if (getVisibleFragment() != null && getVisibleFragment() instanceof HomeFragment) {

            showExitDialog("Are you sure want to exit from Carrotapp ?");
        } else if (fragmentManager.getBackStackEntryCount() > 1) {

            super.onBackPressed();
            if (getVisibleFragment() instanceof HomeFragment) {
                onViewClicked(homeRL);
            } else if (getVisibleFragment() instanceof FeedbackFragment) {
                onViewClicked(feedbackRL);
            } else if (getVisibleFragment() instanceof NoHouseKeepingFragment) {
                onViewClicked(benefitsRL);
            } else if (getVisibleFragment() instanceof VideoFragment) {
                onBackPressed();
            }
        }
    }


    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = HomeActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }


    private void clearBackStack() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }


    private void showExitDialog(String msg) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(msg);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                getString(R.string.yes_txt),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        mySharedPreferences.deleteAllKeyFromPreference();
                        dialog.dismiss();
                        finishAffinity();

                    }
                });

        builder1.setNegativeButton(
                getString(R.string.no_txt),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void handleButtonClicks(int position) {


        homeRL.setBackgroundColor(ContextCompat.getColor(this, R.color.background_cards));
        homeIV.setImageResource(R.drawable.home_inactive);

        feedbackRL.setBackgroundColor(ContextCompat.getColor(this, R.color.background_cards));
        feedbackIV.setImageResource(R.drawable.feedback_inactive);

        benefitsRL.setBackgroundColor(ContextCompat.getColor(this, R.color.background_cards));
        benefitIV.setImageResource(R.drawable.benefit_inactive);

        menuRL.setBackgroundColor(ContextCompat.getColor(this, R.color.background_cards));
        menuIV.setImageResource(R.drawable.menu_inactive);

        switch (position) {
            case 0:
                feedbackRL.setBackgroundColor(ContextCompat.getColor(this, R.color.unselected_menu_color));
                feedbackIV.setImageResource(R.drawable.feedback_active);
                weatherParentLL.setVisibility(View.GONE);
                clockLayout.setVisibility(View.GONE);
                assignLayoutWeight(5.0f, 0.0f);
                break;
            case 1:
                benefitsRL.setBackgroundColor(ContextCompat.getColor(this, R.color.unselected_menu_color));
                benefitIV.setImageResource(R.drawable.benefit);
                weatherParentLL.setVisibility(View.GONE);
                clockLayout.setVisibility(View.GONE);
                assignLayoutWeight(5.0f, 0.0f);
                break;
            case 2:
                menuRL.setBackgroundColor(ContextCompat.getColor(this, R.color.unselected_menu_color));
                menuIV.setImageResource(R.drawable.menu);
                weatherParentLL.setVisibility(View.GONE);
                clockLayout.setVisibility(View.GONE);

                assignLayoutWeight(5.0f, 0.0f);
                break;
            default:
                homeRL.setBackgroundColor(ContextCompat.getColor(this, R.color.unselected_menu_color));
                homeIV.setImageResource(R.drawable.home);

                weatherParentLL.setVisibility(View.VISIBLE);
                clockLayout.setVisibility(View.VISIBLE);
                assignLayoutWeight(4.0f, 1.0f);
                break;
        }
    }

    private void assignLayoutWeight(float right, float left) {
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                0,
                RelativeLayout.LayoutParams.MATCH_PARENT,
                right
        );
        layoutRL.setLayoutParams(param);


        LinearLayout.LayoutParams leftparam = new LinearLayout.LayoutParams(
                0,
                RelativeLayout.LayoutParams.MATCH_PARENT,
                left
        );
        weatherParentLL.setLayoutParams(leftparam);


    }


    private void startClock() {
        timerHandler = new Handler();
        timerRunnable = new Runnable() {

            @Override
            public void run() {

                dateTV.setText(CheckNullable.checkNullable(TimeHelper.getInstance().getCurrentDate()));
                timeTV.setText(CheckNullable.checkNullable(TimeHelper.getInstance().getCurrentTime()));
                ampmTV.setText(" " + CheckNullable.checkNullable(TimeHelper.getInstance().getCurrentAMPM()));
                timerHandler.postDelayed(this, 1000);
            }
        };
        timerHandler.postDelayed(timerRunnable, 0);
    }


    private void startTimerToRefreshWeather() {
        weatherHandler = new Handler();
        weatherRunnable = new Runnable() {

            @Override
            public void run() {
                hitCurrentWeatherApi();
//                15 minutes    //900000
                weatherHandler.postDelayed(this, 900000);
            }
        };
        weatherHandler.postDelayed(weatherRunnable, 900000);
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        Log.d(TAG, "onUserInteraction: ");
        resetDisconnectTimer();
    }


    private Handler disconnectHandler = new Handler();

    private Runnable disconnectCallback = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "run: User Not Interacting Now ");
            onViewClicked(homeRL);
        }
    };

    public void resetDisconnectTimer() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        disconnectHandler.postDelayed(disconnectCallback, AppConstants.TIMER_OUT_USER_INTERACTION);
    }

    @Override
    protected void onPause() {
        disconnectHandler.removeCallbacks(disconnectCallback);
        super.onPause();
    }
}

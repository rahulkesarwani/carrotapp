package com.carrotapp.ui.home;

import com.carrotapp.baseactivities.base.BasePresenter;
import com.carrotapp.controller.IDataManager;
import com.carrotapp.models.logins.LoginRequestModel;
import com.carrotapp.ui.login.LoginMvpPresenter;
import com.carrotapp.ui.login.LoginMvpView;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 *
 * */
public class HomePresenter<V extends HomeMvpView> extends BasePresenter<V> implements HomeMvpPresenter<V> {


    public HomePresenter(IDataManager IDataManager) {
        super(IDataManager);
    }


    @Override
    public void hitWeatherCurrentApi(String url, HashMap<String, String> querymap) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitCurrentWeatherApi(url, querymap)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        /*  .doOnSubscribe(disposable -> ifViewAttached(view -> view.showLoading(R.string.loading)))
                          .doFinally(() -> ifViewAttached(view -> view.hideLoading()))*/
                        .subscribe(commonResponse -> {
                            getMvpView().onCurrentSuccess(commonResponse);
                        }, throwable -> {
                            getMvpView().onFailure(throwable);
                        })
        );
    }

    @Override
    public void hitForecastWeatherApi(String url, HashMap<String, String> querymap) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitForeCaseWeatherApi(url, querymap)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        /*  .doOnSubscribe(disposable -> ifViewAttached(view -> view.showLoading(R.string.loading)))
                          .doFinally(() -> ifViewAttached(view -> view.hideLoading()))*/
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {
                            getMvpView().onFailure(throwable);
                        })
        );
    }
}

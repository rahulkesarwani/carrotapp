package com.carrotapp.ui.fragments.homeFragments;

import com.carrotapp.baseactivities.base.MvpView;
import com.carrotapp.models.weathers.forecast.ForecastWeatherModel;


import retrofit2.Response;


public interface HomeFragmentMvpView extends MvpView {

    void onSuccess(Response<ForecastWeatherModel> response);

    void onFailure(Throwable error);

}

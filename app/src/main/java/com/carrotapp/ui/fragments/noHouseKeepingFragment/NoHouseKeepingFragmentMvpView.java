package com.carrotapp.ui.fragments.noHouseKeepingFragment;

import com.carrotapp.baseactivities.base.MvpView;
import com.carrotapp.models.CommonResponseModel;

import retrofit2.Response;


public interface NoHouseKeepingFragmentMvpView extends MvpView {

    void onSuccess(Response<CommonResponseModel> response);

    void onFailure(Throwable error);

}

package com.carrotapp.ui.fragments.feedbackFragments;



import com.carrotapp.baseactivities.base.BasePresenter;
import com.carrotapp.models.feedback.FeedbackRequestModel;
import com.carrotapp.ui.fragments.homeFragments.HomeFragmentMvpPresenter;
import com.carrotapp.ui.fragments.homeFragments.HomeFragmentMvpView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 *
 * */
public class FeedbackFragmentPresenter<V extends FeedbackFragmentMvpView> extends BasePresenter<V> implements FeedbackFragmentMvpPresenter<V> {


    public FeedbackFragmentPresenter(com.carrotapp.controller.IDataManager IDataManager) {
        super(IDataManager);
    }

    @Override
    public void hitSubmitFeedbackApi(FeedbackRequestModel requestModel) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitFeedbackApi(requestModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        /*  .doOnSubscribe(disposable -> ifViewAttached(view -> view.showLoading(R.string.loading)))
                          .doFinally(() -> ifViewAttached(view -> view.hideLoading()))*/
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {
                            getMvpView().onFailure(throwable);
                        })
        );
    }
}

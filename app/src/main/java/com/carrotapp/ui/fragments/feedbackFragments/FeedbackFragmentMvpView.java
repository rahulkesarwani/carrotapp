package com.carrotapp.ui.fragments.feedbackFragments;

import com.carrotapp.baseactivities.base.MvpView;
import com.carrotapp.models.CommonResponseModel;

import retrofit2.Response;


public interface FeedbackFragmentMvpView extends MvpView {

    void onSuccess(Response<CommonResponseModel> response);

    void onFailure(Throwable error);

}

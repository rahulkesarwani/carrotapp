package com.carrotapp.ui.fragments.videoFragment;

import android.os.Bundle;

import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.carrotapp.R;
import com.carrotapp.baseactivities.BaseFragment;
import com.carrotapp.utils.AppConstants;
import com.carrotapp.utils.customviews.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rahul on 14.03.2017.
 */

public class VideoFragment extends BaseFragment {

    private static final String TAG = VideoFragment.class.getSimpleName();

    View view;
    @BindView(R.id.playIV)
    ImageView playIV;
    @BindView(R.id.msgTV)
    CustomTextView msgTV;
    private String mComeFrom;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(getFragmentLayout(), container, false);
        setUnBinder(ButterKnife.bind(this, view));

        mComeFrom = getArguments().getString(AppConstants.COME_FROM);
        initUI();
        return view;
    }


    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_video;
    }

    @Override
    protected void initUI() {

        if (mComeFrom.equals(AppConstants.COME_FROM_FEEDBACK)) {

            msgTV.setText(getResources().getString(R.string.thank_you_we_value_your_feedback_we_loved_to_hear_you_what_you_think_about_us));
            playFeedBackVideo();
        } else {
            msgTV.setText(getString(R.string.no_housekeeping_feedback_msg));
            playNoHouseKeepingVideo();
        }

    }

    @Override
    protected void handleNoInternet() {

    }

    @Override
    protected void handleSlowInternet() {

    }

    @Override
    protected void handleServerError() {

    }

    @Override
    protected void handleNoDataFound() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @OnClick(R.id.playIV)
    public void onViewClicked() {

    }


    private void playFeedBackVideo() {

    }

    private void playNoHouseKeepingVideo() {
//        youtubeView.getYouTubePlayerWhenReady(youTubePlayer -> {
//            youTubePlayer.loadVideo("vIgxn9kq5H8", 0);
//        });
    }
}

package com.carrotapp.ui.fragments.noHouseKeepingFragment;


import com.carrotapp.baseactivities.base.MvpPresenter;
import com.carrotapp.models.nohousekeeping.NoHouseKeepingRequestModel;
import com.carrotapp.ui.fragments.homeFragments.HomeFragmentMvpView;

/**
 * Created by rahul on 6/14/2018.
 */

public interface NoHouseKeepingFragmentMvpPresenter<V extends NoHouseKeepingFragmentMvpView>  extends MvpPresenter<V> {
    void hitNoHouseKeepingApi(NoHouseKeepingRequestModel requestModel);
}
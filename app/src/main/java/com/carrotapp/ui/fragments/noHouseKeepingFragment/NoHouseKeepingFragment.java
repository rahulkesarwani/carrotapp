package com.carrotapp.ui.fragments.noHouseKeepingFragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.carrotapp.R;
import com.carrotapp.baseactivities.BaseFragment;
import com.carrotapp.models.CommonResponseModel;
import com.carrotapp.models.nohousekeeping.NoHouseKeepingRequestModel;
import com.carrotapp.ui.fragments.videoFragment.VideoFragment;
import com.carrotapp.ui.home.HomeActivity;
import com.carrotapp.utils.AppConstants;
import com.carrotapp.utils.customviews.CustomEditText;
import com.carrotapp.utils.customviews.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by rahul on 14.03.2017.
 */

public class NoHouseKeepingFragment extends BaseFragment implements NoHouseKeepingFragmentMvpView {

    private static final String TAG = NoHouseKeepingFragment.class.getSimpleName();
    @Inject
    NoHouseKeepingFragmentMvpPresenter<NoHouseKeepingFragmentMvpView> mPresenter;

    View view;
    @BindView(R.id.agreeCB)
    AppCompatCheckBox agreeCB;
    @BindView(R.id.agreementTV)
    CustomTextView agreementTV;
    @BindView(R.id.emailET)
    CustomEditText emailET;
    @BindView(R.id.submitTV)
    CustomTextView submitTV;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(getFragmentLayout(), container, false);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);
        initUI();
        return view;
    }

    private void hitNoHouseKeepingApi() {
        if (checkBeforeApiHit()) {
            NoHouseKeepingRequestModel requestModel = new NoHouseKeepingRequestModel();
            requestModel.setAction("request_nohousekeeping");
            requestModel.setRoomId(getRoomId());
            requestModel.setEmail(emailET.getText().toString().trim());
            mPresenter.hitNoHouseKeepingApi(requestModel);
        }


    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_no_housekeeping;
    }

    @Override
    protected void initUI() {
        initErrorViews(view.findViewById(R.id.error_views_layout));
    }

    @Override
    protected void handleNoInternet() {
        hitNoHouseKeepingApi();
    }

    @Override
    protected void handleSlowInternet() {
        hitNoHouseKeepingApi();
    }

    @Override
    protected void handleServerError() {
        hitNoHouseKeepingApi();
    }

    @Override
    protected void handleNoDataFound() {
        hitNoHouseKeepingApi();
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onSuccess(Response<CommonResponseModel> response) {

        if (isResponseOK((short) response.code())) {
            if (response.body().getSuccess()) {
              //  infoToast("Request Accepted Successfully.");
                VideoFragment videoFragment = new VideoFragment();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.COME_FROM, AppConstants.COME_FROM_NO_HOUSEKEEPING);
                videoFragment.setArguments(bundle);
                ((HomeActivity) getActivity()).changeFragment(videoFragment, AppConstants.VIDEO_FRAGMENT_KEY);
            } else {
                showAlertDialog(response.body().getMessage());
            }
        } else {
            showAlertDialog(response.body().getMessage());
        }

    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @OnClick({R.id.submitTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.submitTV:
                if (validateFields())
                    hitNoHouseKeepingApi();

                break;
        }
    }

    private boolean validateFields() {
        if (emailET.getText().toString().trim().length() == 0) {
            errorToast("Enter Email Id");
            return false;
        } else if (!((HomeActivity) getActivity()).validateEmail(emailET.getText().toString().trim())) {
            errorToast("Enter Valid Email Id");
            return false;
        } /*else if (!agreeCB.isChecked()) {
            errorToast("Please Agree with T & C");
            return false;
        } */else {
            return true;
        }
    }
}

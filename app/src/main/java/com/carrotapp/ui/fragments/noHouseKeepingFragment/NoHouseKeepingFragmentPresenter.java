package com.carrotapp.ui.fragments.noHouseKeepingFragment;



import com.carrotapp.baseactivities.base.BasePresenter;
import com.carrotapp.models.nohousekeeping.NoHouseKeepingRequestModel;
import com.carrotapp.ui.fragments.homeFragments.HomeFragmentMvpPresenter;
import com.carrotapp.ui.fragments.homeFragments.HomeFragmentMvpView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 *
 * */
public class NoHouseKeepingFragmentPresenter<V extends NoHouseKeepingFragmentMvpView> extends BasePresenter<V> implements NoHouseKeepingFragmentMvpPresenter<V> {


    public NoHouseKeepingFragmentPresenter(com.carrotapp.controller.IDataManager IDataManager) {
        super(IDataManager);
    }


    @Override
    public void hitNoHouseKeepingApi(NoHouseKeepingRequestModel requestModel) {
                getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitNoHouseKeepingApi(requestModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        /*  .doOnSubscribe(disposable -> ifViewAttached(view -> view.showLoading(R.string.loading)))
                          .doFinally(() -> ifViewAttached(view -> view.hideLoading()))*/
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {
                            getMvpView().onFailure(throwable);
                        })
        );
    }
}

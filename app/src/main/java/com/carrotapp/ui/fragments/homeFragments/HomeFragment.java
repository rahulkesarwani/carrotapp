package com.carrotapp.ui.fragments.homeFragments;

import android.os.Bundle;

import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.carrotapp.R;
import com.carrotapp.baseactivities.BaseFragment;
import com.carrotapp.models.weathers.current.CurrentWeatherModel;
import com.carrotapp.models.weathers.forecast.ForecastWeatherModel;

import javax.inject.Inject;

import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by rahul on 14.03.2017.
 */

public class HomeFragment extends BaseFragment implements HomeFragmentMvpView {

    private static final String TAG = HomeFragment.class.getSimpleName();
    @Inject
    HomeFragmentMvpPresenter<HomeFragmentMvpView> mPresenter;

    View view;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(getFragmentLayout(), container, false);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);
        initUI();
        return view;
    }

    private void hitHomeApi() {
        if (checkBeforeApiHit()) {

        }


    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initUI() {

        //hitHomeApi();
        
    }

    @Override
    protected void handleNoInternet() {
        hitHomeApi();
    }

    @Override
    protected void handleSlowInternet() {
        hitHomeApi();
    }

    @Override
    protected void handleServerError() {
        hitHomeApi();
    }

    @Override
    protected void handleNoDataFound() {
        hitHomeApi();
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }


    @Override
    public void onSuccess(Response<ForecastWeatherModel> response) {

    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


}

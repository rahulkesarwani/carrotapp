package com.carrotapp.ui.fragments.feedbackFragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.carrotapp.R;
import com.carrotapp.baseactivities.BaseFragment;
import com.carrotapp.models.CommonResponseModel;
import com.carrotapp.models.feedback.FeedbackRequestModel;
import com.carrotapp.ui.fragments.videoFragment.VideoFragment;
import com.carrotapp.ui.home.HomeActivity;
import com.carrotapp.utils.AppConstants;
import com.carrotapp.utils.customviews.CustomEditText;
import com.carrotapp.utils.customviews.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by rahul on 14.03.2017.
 */

public class FeedbackFragment extends BaseFragment implements FeedbackFragmentMvpView {

    private static final String TAG = FeedbackFragment.class.getSimpleName();
    @Inject
    FeedbackFragmentMvpPresenter<FeedbackFragmentMvpView> mPresenter;

    View view;
    @BindView(R.id.overallGoodIV)
    ImageView overallGoodIV;
    @BindView(R.id.overallGoodTV)
    CustomTextView overallGoodTV;
    @BindView(R.id.overAllGoodLL)
    LinearLayout overAllGoodLL;
    @BindView(R.id.overallOkayIV)
    ImageView overallOkayIV;
    @BindView(R.id.overallOkayTV)
    CustomTextView overallOkayTV;
    @BindView(R.id.overAllOkayLL)
    LinearLayout overAllOkayLL;
    @BindView(R.id.overallBadIV)
    ImageView overallBadIV;
    @BindView(R.id.overallBadTV)
    CustomTextView overallBadTV;
    @BindView(R.id.overAllBadLL)
    LinearLayout overAllBadLL;
    @BindView(R.id.inroomGoodIV)
    ImageView inroomGoodIV;
    @BindView(R.id.inroomGoodTV)
    CustomTextView inroomGoodTV;
    @BindView(R.id.inroomGoodLL)
    LinearLayout inroomGoodLL;
    @BindView(R.id.inroomOkayIV)
    ImageView inroomOkayIV;
    @BindView(R.id.inroomOkayTV)
    CustomTextView inroomOkayTV;
    @BindView(R.id.inroomOkayLL)
    LinearLayout inroomOkayLL;
    @BindView(R.id.inroomBadIV)
    ImageView inroomBadIV;
    @BindView(R.id.inroomBadTV)
    CustomTextView inroomBadTV;
    @BindView(R.id.inroomBadLL)
    LinearLayout inroomBadLL;
    @BindView(R.id.checkinGoodIV)
    ImageView checkinGoodIV;
    @BindView(R.id.checkinGoodTV)
    CustomTextView checkinGoodTV;
    @BindView(R.id.checkinGoodLL)
    LinearLayout checkinGoodLL;
    @BindView(R.id.checkinOkayIV)
    ImageView checkinOkayIV;
    @BindView(R.id.checkinOkayTV)
    CustomTextView checkinOkayTV;
    @BindView(R.id.checkinOkayLL)
    LinearLayout checkinOkayLL;
    @BindView(R.id.checkinBadIV)
    ImageView checkinBadIV;
    @BindView(R.id.checkinBadTV)
    CustomTextView checkinBadTV;
    @BindView(R.id.checkinBadLL)
    LinearLayout checkinBadLL;
    @BindView(R.id.bathroomGoodIV)
    ImageView bathroomGoodIV;
    @BindView(R.id.bathroomGoodTV)
    CustomTextView bathroomGoodTV;
    @BindView(R.id.bathroomGoodLL)
    LinearLayout bathroomGoodLL;
    @BindView(R.id.bathroomOkayIV)
    ImageView bathroomOkayIV;
    @BindView(R.id.bathroomOkayTV)
    CustomTextView bathroomOkayTV;
    @BindView(R.id.bathroomOkayLL)
    LinearLayout bathroomOkayLL;
    @BindView(R.id.bathroomBadIV)
    ImageView bathroomBadIV;
    @BindView(R.id.bathroomBadTV)
    CustomTextView bathroomBadTV;
    @BindView(R.id.bathroomBadLL)
    LinearLayout bathroomBadLL;
    @BindView(R.id.otherGoodIV)
    ImageView otherGoodIV;
    @BindView(R.id.otherGoodTV)
    CustomTextView otherGoodTV;
    @BindView(R.id.otherGoodLL)
    LinearLayout otherGoodLL;
    @BindView(R.id.otherOkayIV)
    ImageView otherOkayIV;
    @BindView(R.id.otherOkayTV)
    CustomTextView otherOkayTV;
    @BindView(R.id.otherOkayLL)
    LinearLayout otherOkayLL;
    @BindView(R.id.otherBadIV)
    ImageView otherBadIV;
    @BindView(R.id.otherBadTV)
    CustomTextView otherBadTV;
    @BindView(R.id.otherBadLL)
    LinearLayout otherBadLL;
    @BindView(R.id.foodQualityGoodIV)
    ImageView foodQualityGoodIV;
    @BindView(R.id.foodQualityGoodTV)
    CustomTextView foodQualityGoodTV;
    @BindView(R.id.foodQualityGoodLL)
    LinearLayout foodQualityGoodLL;
    @BindView(R.id.foodQualityOkayIV)
    ImageView foodQualityOkayIV;
    @BindView(R.id.foodQualityOkayTV)
    CustomTextView foodQualityOkayTV;
    @BindView(R.id.foodQualityOkayLL)
    LinearLayout foodQualityOkayLL;
    @BindView(R.id.foodQualityBadIV)
    ImageView foodQualityBadIV;
    @BindView(R.id.foodQualityBadTV)
    CustomTextView foodQualityBadTV;
    @BindView(R.id.foodQualityBadLL)
    LinearLayout foodQualityBadLL;
    @BindView(R.id.staffFriendinessGoodIV)
    ImageView staffFriendinessGoodIV;
    @BindView(R.id.staffFriendinessGoodTV)
    CustomTextView staffFriendinessGoodTV;
    @BindView(R.id.staffFriendinessGoodLL)
    LinearLayout staffFriendinessGoodLL;
    @BindView(R.id.staffFriendinessOkayIV)
    ImageView staffFriendinessOkayIV;
    @BindView(R.id.staffFriendinessOkayTV)
    CustomTextView staffFriendinessOkayTV;
    @BindView(R.id.staffFriendinessOkayLL)
    LinearLayout staffFriendinessOkayLL;
    @BindView(R.id.staffFriendinessBadIV)
    ImageView staffFriendinessBadIV;
    @BindView(R.id.staffFriendinessBadTV)
    CustomTextView staffFriendinessBadTV;
    @BindView(R.id.staffFriendinessBadLL)
    LinearLayout staffFriendinessBadLL;
    @BindView(R.id.commentET)
    CustomEditText commentET;
    @BindView(R.id.submitTV)
    CustomTextView submitTV;


    private CustomTextView textView[][];
    private ImageView imageView[][];
    private FeedbackRequestModel requestModel;

    private boolean isAnyCommentClicked;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(getFragmentLayout(), container, false);

        getActivityComponent().inject(this);

        setUnBinder(ButterKnife.bind(this, view));

        mPresenter.onAttach(this);
        textView = new CustomTextView[7][3];
        imageView = new ImageView[7][3];
        requestModel = new FeedbackRequestModel();
        initUI();
        return view;
    }

    private void hitFeedbackSubmitApi() {
        if (checkBeforeApiHit()) {
            requestModel.setAction("feedback");

            requestModel.setHotelId(AppConstants.HOTEL_ID);
            requestModel.setRoomId(Integer.parseInt(getRoomId()));
            requestModel.setComment(commentET.getText().toString().trim());
            mPresenter.hitSubmitFeedbackApi(requestModel);
        }


    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_feedback;
    }

    @Override
    protected void initUI() {

        initErrorViews(view.findViewById(R.id.error_views_layout));

        textView[0][0] = overallGoodTV;
        textView[0][1] = overallOkayTV;
        textView[0][2] = overallBadTV;

        textView[1][0] = inroomGoodTV;
        textView[1][1] = inroomOkayTV;
        textView[1][2] = inroomBadTV;

        textView[2][0] = checkinGoodTV;
        textView[2][1] = checkinOkayTV;
        textView[2][2] = checkinBadTV;

        textView[3][0] = bathroomGoodTV;
        textView[3][1] = bathroomOkayTV;
        textView[3][2] = bathroomBadTV;

        textView[4][0] = otherGoodTV;
        textView[4][1] = otherOkayTV;
        textView[4][2] = otherBadTV;

        textView[5][0] = foodQualityGoodTV;
        textView[5][1] = foodQualityOkayTV;
        textView[5][2] = foodQualityBadTV;

        textView[6][0] = staffFriendinessGoodTV;
        textView[6][1] = staffFriendinessOkayTV;
        textView[6][2] = staffFriendinessBadTV;

        imageView[0][0] = overallGoodIV;
        imageView[0][1] = overallOkayIV;
        imageView[0][2] = overallBadIV;

        imageView[1][0] = inroomGoodIV;
        imageView[1][1] = inroomOkayIV;
        imageView[1][2] = inroomBadIV;

        imageView[2][0] = checkinGoodIV;
        imageView[2][1] = checkinOkayIV;
        imageView[2][2] = checkinBadIV;

        imageView[3][0] = bathroomGoodIV;
        imageView[3][1] = bathroomOkayIV;
        imageView[3][2] = bathroomBadIV;

        imageView[4][0] = otherGoodIV;
        imageView[4][1] = otherOkayIV;
        imageView[4][2] = otherBadIV;

        imageView[5][0] = foodQualityGoodIV;
        imageView[5][1] = foodQualityOkayIV;
        imageView[5][2] = foodQualityBadIV;

        imageView[6][0] = staffFriendinessGoodIV;
        imageView[6][1] = staffFriendinessOkayIV;
        imageView[6][2] = staffFriendinessBadIV;

    }

    @Override
    protected void handleNoInternet() {
        hitFeedbackSubmitApi();
    }

    @Override
    protected void handleSlowInternet() {
        hitFeedbackSubmitApi();
    }

    @Override
    protected void handleServerError() {
        hitFeedbackSubmitApi();
    }

    @Override
    protected void handleNoDataFound() {
        hitFeedbackSubmitApi();
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onSuccess(Response<CommonResponseModel> response) {

        if (isResponseOK((short) response.code())) {
            if (response.body().getSuccess()) {
                VideoFragment videoFragment = new VideoFragment();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.COME_FROM, AppConstants.COME_FROM_FEEDBACK);
                videoFragment.setArguments(bundle);
                ((HomeActivity) getActivity()).changeFragment(videoFragment, AppConstants.VIDEO_FRAGMENT_KEY);
            } else {
                showAlertDialog(response.body().getMessage());
            }
        } else {
            showAlertDialog(response.body().getMessage());
        }

    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @OnClick({R.id.overAllGoodLL, R.id.overAllOkayLL, R.id.overAllBadLL, R.id.inroomGoodLL, R.id.inroomOkayLL, R.id.inroomBadLL, R.id.checkinGoodLL, R.id.checkinOkayLL, R.id.checkinBadLL, R.id.bathroomGoodLL, R.id.bathroomOkayLL, R.id.bathroomBadLL, R.id.otherGoodLL, R.id.otherOkayLL, R.id.otherBadLL, R.id.foodQualityGoodLL, R.id.foodQualityOkayLL, R.id.foodQualityBadLL, R.id.staffFriendinessGoodLL, R.id.staffFriendinessOkayLL, R.id.staffFriendinessBadLL, R.id.submitTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.overAllGoodLL:
                requestModel.setOverall(0);
                changeBg(0, 0);
                break;
            case R.id.overAllOkayLL:
                requestModel.setOverall(1);
                changeBg(0, 1);
                break;
            case R.id.overAllBadLL:
                requestModel.setOverall(2);
                changeBg(0, 2);
                break;
            case R.id.inroomGoodLL:
                requestModel.setInRoom(0);
                changeBg(1, 0);
                break;
            case R.id.inroomOkayLL:
                requestModel.setInRoom(1);
                changeBg(1, 1);
                break;
            case R.id.inroomBadLL:
                requestModel.setInRoom(2);
                changeBg(1, 2);
                break;
            case R.id.checkinGoodLL:
                requestModel.setCheckIn(0);
                changeBg(2, 0);
                break;
            case R.id.checkinOkayLL:
                requestModel.setCheckIn(1);
                changeBg(2, 1);
                break;
            case R.id.checkinBadLL:
                requestModel.setCheckIn(2);
                changeBg(2, 2);
                break;
            case R.id.bathroomGoodLL:
                requestModel.setBathroom(0);
                changeBg(3, 0);
                break;
            case R.id.bathroomOkayLL:
                requestModel.setBathroom(1);
                changeBg(3, 1);
                break;
            case R.id.bathroomBadLL:
                requestModel.setBathroom(2);
                changeBg(3, 2);
                break;
            case R.id.otherGoodLL:
                requestModel.setOther(0);
                changeBg(4, 0);
                break;
            case R.id.otherOkayLL:
                requestModel.setOther(1);
                changeBg(4, 1);
                break;
            case R.id.otherBadLL:
                requestModel.setOther(2);
                changeBg(4, 2);
                break;
            case R.id.foodQualityGoodLL:
                requestModel.setFoodQuality(0);
                changeBg(5, 0);
                break;
            case R.id.foodQualityOkayLL:
                requestModel.setFoodQuality(1);
                changeBg(5, 1);
                break;
            case R.id.foodQualityBadLL:
                requestModel.setFoodQuality(2);
                changeBg(5, 2);
                break;
            case R.id.staffFriendinessGoodLL:
                requestModel.setStaffFriendiness(0);
                changeBg(6, 0);
                break;
            case R.id.staffFriendinessOkayLL:
                requestModel.setStaffFriendiness(1);
                changeBg(6, 1);
                break;
            case R.id.staffFriendinessBadLL:
                requestModel.setStaffFriendiness(2);
                changeBg(6, 2);
                break;
            case R.id.submitTV:
                if (validateFields()) {
                    hitFeedbackSubmitApi();
                }

                break;
        }
    }


    private void changeBg(int row, int selectedColumn) {

        isAnyCommentClicked = true;
//         make unselect
        for (int i = 0; i < 3; i++) {
            textView[row][i].setTextColor(ContextCompat.getColor(getContext(), R.color.dim_white_color));
        }
        imageView[row][0].setImageResource(R.drawable.good_inactive);
        imageView[row][1].setImageResource(R.drawable.okay_inactive);
        imageView[row][2].setImageResource(R.drawable.bad_inactive);

//        make select now

        textView[row][selectedColumn].setTextColor(ContextCompat.getColor(getContext(), R.color.colorPureWhite));
        switch (selectedColumn) {
            case 0:
                imageView[row][selectedColumn].setImageResource(R.drawable.good);
                break;
            case 1:
                imageView[row][selectedColumn].setImageResource(R.drawable.okay);
                break;
            case 2:
                imageView[row][selectedColumn].setImageResource(R.drawable.bad);
                break;
            default:
                throw new IllegalArgumentException("Not Valid Argument");
        }
    }


    private boolean validateFields() {
        if (!isAnyCommentClicked) {
            showAlertDialog("Please Select Atleast one feedback");
            return false;
        } else {
            return true;
        }
    }

}

package com.carrotapp.ui.fragments.feedbackFragments;


import com.carrotapp.baseactivities.base.MvpPresenter;
import com.carrotapp.models.feedback.FeedbackRequestModel;
import com.carrotapp.ui.fragments.homeFragments.HomeFragmentMvpView;

/**
 * Created by rahul on 6/14/2018.
 */

public interface FeedbackFragmentMvpPresenter<V extends FeedbackFragmentMvpView>  extends MvpPresenter<V> {
    void hitSubmitFeedbackApi(FeedbackRequestModel requestModel);
}
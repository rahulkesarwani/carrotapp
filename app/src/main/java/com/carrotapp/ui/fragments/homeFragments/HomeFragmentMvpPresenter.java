package com.carrotapp.ui.fragments.homeFragments;


import com.carrotapp.baseactivities.base.MvpPresenter;

import java.util.HashMap;

/**
 * Created by rahul on 6/14/2018.
 */

public interface HomeFragmentMvpPresenter<V extends HomeFragmentMvpView>  extends MvpPresenter<V> {
    void hitWeatherApi(String url, HashMap<String,String> header,HashMap<String,String> querymap);
}
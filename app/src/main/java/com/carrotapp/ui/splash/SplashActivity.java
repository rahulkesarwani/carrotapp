package com.carrotapp.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.carrotapp.R;
import com.carrotapp.ui.home.HomeActivity;
import com.carrotapp.ui.login.LoginActivity;
import com.carrotapp.utils.AppConstants;
import com.carrotapp.utils.MySharedPreferences;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.logoIV)
    ImageView logoIV;

    private int SPLASH_DELAY = 2300;
    private Animation anim;
    private MySharedPreferences mySharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        mySharedPreferences=new MySharedPreferences(this);
        startAnimation();

        new Handler().postDelayed(() -> {
            anim.cancel();
            boolean isUserLoggedIn = mySharedPreferences.getBoolean(AppConstants.IS_USER_LOGGED_IN, false);

            Intent intent = null;
            if (isUserLoggedIn) {
                intent = new Intent(SplashActivity.this, HomeActivity.class);
            } else {
                intent = new Intent(SplashActivity.this, LoginActivity.class);
            }
            startActivity(intent);
            overridePendingTransition(R.anim.zoom_out, R.anim.no_change);
            finish();
        }, SPLASH_DELAY);

    }



    public void startAnimation() {


        anim = AnimationUtils.loadAnimation(this, R.anim.logo_animator);
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationEnd(Animation arg0) {
                anim = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.logo_animator);
                anim.setAnimationListener(this);
                logoIV.startAnimation(anim);
            }

            @Override
            public void onAnimationRepeat(Animation arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationStart(Animation arg0) {
                // TODO Auto-generated method stub

            }

        });
        logoIV.startAnimation(anim);
    }

}

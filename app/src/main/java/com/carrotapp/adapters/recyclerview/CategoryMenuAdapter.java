package com.carrotapp.adapters.recyclerview;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.carrotapp.R;
import com.carrotapp.models.menus.CategoryModel;
import com.carrotapp.utils.CheckNullable;
import com.carrotapp.utils.customviews.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryMenuAdapter extends RecyclerView.Adapter {
    private final String TAG = CategoryMenuAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;


    private Context mContext = null;
    private List<CategoryModel> list;
    private int selectedCategoryPosition = 0;
    private OnSelectedCategoryListener mlistener;

    public CategoryMenuAdapter(Context mContext, List<CategoryModel> list, OnSelectedCategoryListener mListener) {

        this.mContext = mContext;
        this.list = list;
        this.mlistener = mListener;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_category_menu, parent, false);
            vh = new CategoryMenuViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof CategoryMenuViewHolder) {

            if (selectedCategoryPosition == position) {
                ((CategoryMenuViewHolder) holder).categoryTV.setBackground(ContextCompat.getDrawable(mContext, R.drawable.background_category_menu_selected));
            } else {
                ((CategoryMenuViewHolder) holder).categoryTV.setBackground(ContextCompat.getDrawable(mContext, R.drawable.background_category_menu));

            }

            ((CategoryMenuViewHolder) holder).categoryTV.setText(CheckNullable.checkNullable(list.get(position).getName()));

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class CategoryMenuViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.categoryTV)
        CustomTextView categoryTV;

        public CategoryMenuViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

            categoryTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mlistener == null)
                        return;
                    mlistener.onCategorySelectedPosition(getAdapterPosition());

                    selectedCategoryPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBarPagination);
        }
    }


    public interface OnSelectedCategoryListener {
        void onCategorySelectedPosition(int position);
    }
}

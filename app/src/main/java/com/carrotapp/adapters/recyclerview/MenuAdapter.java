package com.carrotapp.adapters.recyclerview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.carrotapp.FullScreenImageActivity;
import com.carrotapp.PhotoFullPopupWindow;
import com.carrotapp.R;
import com.carrotapp.models.menus.MenuResponseModel;
import com.carrotapp.ui.menus.MenuActivity;
import com.carrotapp.ui.videoplayer.VideoPlayerActivity;
import com.carrotapp.utils.AppConstants;
import com.carrotapp.utils.CheckNullable;
import com.carrotapp.utils.GlideUtils;
import com.carrotapp.utils.Helper;
import com.carrotapp.utils.customviews.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuAdapter extends RecyclerView.Adapter {
    private final String TAG = MenuAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;


    private Context mContext = null;
    private List<MenuResponseModel> list = new ArrayList<>();

    public MenuAdapter(Context mContext, List<MenuResponseModel> list) {

        this.mContext = mContext;
        this.list = list;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_menu, parent, false);
            vh = new MenuViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof MenuViewHolder) {

            if (list.get(position).getSubmenus().size() > 0) {
                ((MenuViewHolder) holder).priceTV.setVisibility(View.GONE);
                ((MenuViewHolder) holder).subMenuLL.setVisibility(View.VISIBLE);
                SubMenuAdapter subMenuAdapter = new SubMenuAdapter(mContext, list.get(position).getSubmenus());
                ((MenuViewHolder) holder).submenuRV.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                ((MenuViewHolder) holder).submenuRV.setAdapter(subMenuAdapter);
            } else {
                ((MenuViewHolder) holder).subMenuLL.setVisibility(View.GONE);
                ((MenuViewHolder) holder).priceTV.setVisibility(View.VISIBLE);
            }

            ((MenuViewHolder) holder).dishNameTV.setText(CheckNullable.checkNullable(list.get(position).getName()));
            ((MenuViewHolder) holder).descriptionTV.setText(CheckNullable.checkNullable(list.get(position).getDescription()));


            if (!TextUtils.isEmpty(list.get(position).getVideo())) {
                GlideUtils.loadImage(mContext, AppConstants.mVideoBaseUrl + list.get(position).getVideo(), ((MenuViewHolder) holder).thumbnailIV, R.color.background_header);
                ((MenuViewHolder) holder).playVideoRL.setVisibility(View.VISIBLE);
            } else {
                ((MenuViewHolder) holder).playVideoRL.setVisibility(View.GONE);

            }

            GlideUtils.loadImage(mContext, AppConstants.mImageBaseUrl + list.get(position).getImage(), ((MenuViewHolder) holder).dishIV, R.drawable.food_sample);

            ((MenuViewHolder) holder).priceTV.setText("$ " + CheckNullable.checkNullable(list.get(position).getPrice()));

            ViewCompat.setTransitionName(((MenuViewHolder) holder).dishIV, String.valueOf(list.get(position).getId()));

            ((MenuViewHolder) holder).dishIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, FullScreenImageActivity.class);
                    intent.putExtra(AppConstants.CHAT_PIC_URL, AppConstants.mImageBaseUrl + list.get(position).getImage());
                    intent.putExtra(AppConstants.EXTRA_ANIMAL_IMAGE_TRANSITION_NAME, ViewCompat.getTransitionName(((MenuViewHolder) holder).dishIV));

                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                            (MenuActivity) mContext,
                            ((MenuViewHolder) holder).dishIV,
                            ViewCompat.getTransitionName(((MenuViewHolder) holder).dishIV));

                    mContext.startActivity(intent, options.toBundle());

                    //  PhotoFullPopupWindow photoFullPopupWindow=new PhotoFullPopupWindow(mContext,((MenuViewHolder) holder).dishIV,AppConstants.mImageBaseUrl + list.get(position).getImage(),null);
                }
            });
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MenuViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.dishNameTV)
        CustomTextView dishNameTV;
        @BindView(R.id.descriptionTV)
        CustomTextView descriptionTV;
        @BindView(R.id.dishIV)
        ImageView dishIV;
        @BindView(R.id.thumbnailIV)
        ImageView thumbnailIV;
        @BindView(R.id.playVideoRL)
        CardView playVideoRL;
        @BindView(R.id.priceTV)
        CustomTextView priceTV;
        @BindView(R.id.submenuRV)
        RecyclerView submenuRV;
        @BindView(R.id.subMenuLL)
        LinearLayout subMenuLL;

        public MenuViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);


            playVideoRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, VideoPlayerActivity.class);
                    intent.putExtra("videoUrl", list.get(getAdapterPosition()).getVideo());
                    mContext.startActivity(intent);
                }
            });
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBarPagination);
        }
    }

}

package com.carrotapp.adapters.recyclerview;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.carrotapp.R;
import com.carrotapp.models.menus.Submenu;
import com.carrotapp.utils.CheckNullable;
import com.carrotapp.utils.customviews.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubMenuAdapter extends RecyclerView.Adapter {
    private final String TAG = SubMenuAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;


    private Context mContext = null;
    private List<Submenu> list = new ArrayList<>();


    public SubMenuAdapter(Context mContext, List<Submenu> list) {

        this.mContext = mContext;
        this.list = list;

    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_submenu, parent, false);
            vh = new SubMenuViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof SubMenuViewHolder) {

            ((SubMenuViewHolder) holder).typeTV.setText(CheckNullable.checkNullable(list.get(position).getName()));
            ((SubMenuViewHolder) holder).priceTV.setText("$ " + CheckNullable.checkNullable(list.get(position).getPrice()));
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class SubMenuViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.typeTV)
        CustomTextView typeTV;
        @BindView(R.id.priceTV)
        CustomTextView priceTV;

        public SubMenuViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBarPagination);
        }
    }

}

package com.carrotapp.apis;

import com.carrotapp.models.CommonResponseModel;
import com.carrotapp.models.feedback.FeedbackRequestModel;
import com.carrotapp.models.logins.LoginRequestModel;
import com.carrotapp.models.logins.LoginResponseModel;
import com.carrotapp.models.menus.MenuRequestModel;
import com.carrotapp.models.nohousekeeping.NoHouseKeepingRequestModel;
import com.carrotapp.models.weathers.current.CurrentWeatherModel;
import com.carrotapp.models.weathers.forecast.ForecastWeatherModel;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;


/**
 * Created by innotical on 28/6/17.
 */

public interface ApiInterface {


    //    @GET("/session/")
//    public Call<SessionModel> getSessionDetails(@Query("id") int id);

    @Headers("Content-Type:application/json")
    @POST("Api.php")
    Observable<Response<CommonResponseModel>> hitLoginApi(@Body LoginRequestModel loginRequestModel);

    @Headers("Content-Type:application/json")
    @POST("user/signup/")
    Observable<Response<LoginResponseModel>> hitPostAdd(@Body MultipartBody request);

    @Headers("Content-Type:application/json")
    @POST("Api.php")
    Observable<Response<CommonResponseModel>> hitFeedbackApi(@Body FeedbackRequestModel requestModel);

    @Headers("Content-Type:application/json")
    @POST("Api.php")
    Observable<Response<CommonResponseModel>> hitNoHouseKeepingApi(@Body NoHouseKeepingRequestModel requestModel);

    @Headers("Content-Type:application/json")
    @POST("Api.php")
    Observable<Response<CommonResponseModel>> hitMenuListApi(@Body MenuRequestModel requestModel);

    @Headers("Content-Type:application/json")
    @POST("Api.php")
    Observable<Response<CommonResponseModel>> hitCategoryApi(@Body MenuRequestModel requestModel);

    @Headers("Content-Type:application/json")
    @GET
    Observable<Response<List<CurrentWeatherModel>>> hitCurrentWeatherApi(@Url String url, @QueryMap HashMap<String, String> querymap);

    @Headers("Content-Type:application/json")
    @GET
    Observable<Response<ForecastWeatherModel>> hitForeCaseWeatherApi(@Url String url, @QueryMap HashMap<String, String> querymap);

}
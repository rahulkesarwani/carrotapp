package com.carrotapp.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.carrotapp.models.logins.LoginResponseModel;

/**
 * Created by rahul on 1/10/2018.
 */

public class Helper {

    private MySharedPreferences mySharedPreferences;

    private static Helper instance;

    public static Helper getInstance() {
        if (instance == null)
            instance = new Helper();
        return instance;
    }

    public void makeCall(Context context, String contactNo) {

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + contactNo));
        context.startActivity(intent);
    }

    public void makeCall(Context context) {

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + getContactNo(context)));
        context.startActivity(intent);
    }


    public String getContactNo(Context context) {

        mySharedPreferences=new MySharedPreferences(context);
        LoginResponseModel responseModel = ((LoginResponseModel) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, LoginResponseModel.class));
        if (responseModel != null)
            return responseModel.getHotel().getContactNo();
        else
            return "";

    }

}


package com.carrotapp.utils;

/**
 * Created by rahul on 1/5/2018.
 */

public interface AppConstants {

    /*Urls */

    String mBaseUrl = "http://agiletechsoltions.com.au/carrot_backend/";
    String mImageBaseUrl = "http://agiletechsoltions.com.au/carrot_backend/images/";
    String mVideoBaseUrl = "http://agiletechsoltions.com.au/carrot_backend/videos/";
    String mRegisterDeviceUrl = "http://apipush.sia.co.in/api/";
    String mRefreshTokenBaseURL = "https://apiv3.myswaasth.com/o/";
    String mBaseWSS = "wss://apiv3.myswaasth.com/";


    /**/
    String ADDRESS = "CURRENT_ADDRESS";
    String LATITUDE = "CURRENT_LATITUDE";
    String LONGITUDE = "CURRENT_LONGITUDE";
    String ACCESS_TOKEN = "ACCESS_TOKEN";

    /*for refresh token*/
    String FIRST_TIME_LOGIN_KEY = "firstTimeLoginTime";
    String CLIENT_ID = "client_id";
    String CLIENT_SECRET = "client_secret";
    String TOKEN_EXPIRY_TIME = "expires_in";
    String REFRESH_TOKEN = "refresh_token";
    String mGrantType = "refresh_token";
    String EXTRA_ANIMAL_IMAGE_TRANSITION_NAME = "EXTRA_ANIMAL_IMAGE_TRANSITION_NAME";


    int HOTEL_ID = 1;


    /*Actions Constants*/
    String LOGIN_ACTION = "login";
    String LOGIN_DATA = "LOGIN_DATA";
    String HOME_FRAGMENT_KEY = "HOME_FRAGMENT_KEY";
    String FEEDBACK_FRAGMENT_KEY = "FEEDBACK_FRAGMENT_KEY";
    String NO_HOUSE_KEEPING = "NO_HOUSE_KEEPING";
    String COME_FROM = "COME_FROM";
    String COME_FROM_FEEDBACK = "COME_FROM_FEEDBACK";
    String COME_FROM_NO_HOUSEKEEPING = "COME_FROM_NO_HOUSEKEEPING";
    String VIDEO_FRAGMENT_KEY = "VIDEO_FRAGMENT_KEY";
    String IS_USER_LOGGED_IN = "IS_USER_LOGGED_IN";

    String DATE_FORMAT_WEATHER = "EE, dd MMMM";
    String CHAT_PIC_URL = "CHAT_PIC_URL";


    int TIMER_OUT_USER_INTERACTION=60000;
}

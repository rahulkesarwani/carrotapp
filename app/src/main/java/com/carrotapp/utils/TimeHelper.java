package com.carrotapp.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by rahul on 4/9/17.
 */

public class TimeHelper {


    private String TAG = "TimeHelper";
    Calendar cal;
    // private TimeFormatHelper timeFormatHelper;
    private static TimeHelper instance;

    public TimeHelper() {
        //timeFormatHelper = new TimeFormatHelper();
    }


    public static TimeHelper getInstance() {
        if (instance == null)
            instance = new TimeHelper();
        return instance;
    }

    public String getCurrentDate() {
        SimpleDateFormat sf = new SimpleDateFormat("EE, dd MMMM yyyy");
        Date date = new Date(System.currentTimeMillis());
        return sf.format(date);
    }

    public String getCurrentTime() {
        SimpleDateFormat sf = new SimpleDateFormat("hh:mm");
        Date date = new Date(System.currentTimeMillis());
        return sf.format(date);
    }

    public String getCurrentAMPM() {
        SimpleDateFormat sf = new SimpleDateFormat("a");
        Date date = new Date(System.currentTimeMillis());
        return sf.format(date);
    }

    public String convertDateFromTimeStamp(String timestamp, String desiredFormat) {
        long times = Long.parseLong(timestamp);
        SimpleDateFormat sf = new SimpleDateFormat(desiredFormat);
        Date date = new Date((long) times * 1000);
        return sf.format(date);
    }

   /* public String convertDesiredDate(String date, String desiredFormat) throws ParseException {

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
        Date result;
        result = df.parse(date);
        SimpleDateFormat sdf = new SimpleDateFormat(desiredFormat);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(result);

    }*/


    public String getTodayDate() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat(AppConstants.DATE_FORMAT_WEATHER);
        String todayAsString = dateFormat.format(today);
        return todayAsString;
    }

    public String getTomorrowDate() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat(AppConstants.DATE_FORMAT_WEATHER);
        String tomorrowAsString = dateFormat.format(tomorrow);
        return tomorrowAsString;
    }

    public String getDayAfterTomorrowDate() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 2);
        Date tomorrow = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat(AppConstants.DATE_FORMAT_WEATHER);
        String tomorrowAsString = dateFormat.format(tomorrow);
        return tomorrowAsString;
    }

    public boolean isNight() {
        Boolean isNight;
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        if (hour < 6 || hour > 18) {
            isNight = true;
        } else {
            isNight = false;
        }
        return isNight;
    }

}

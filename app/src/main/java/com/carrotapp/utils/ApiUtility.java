package com.carrotapp.utils;

import com.carrotapp.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rahul on 1/5/2018.
 */

public class ApiUtility {

    private static ApiUtility instance = null;

    public static ApiUtility getInstance() {
        if (instance != null) {
            return instance;
        } else {
            instance = new ApiUtility();
            return instance;
        }
    }

    public Retrofit getRetrofit() {

        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();


        Gson gson = new GsonBuilder()
                .setLenient()
                .serializeNulls()
                .create();

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        /**/
        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .build();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(1, TimeUnit.MINUTES);

        httpClient.addInterceptor(logging);


        Retrofit.Builder mBuilder = new Retrofit.Builder();

        if (BuildConfig.DEBUG) {
            mBuilder.client(httpClient.build());
        }

        Retrofit retrofit = mBuilder
                .baseUrl(AppConstants.mBaseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())  // <- add this  for rx java
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit;
    }

    public Retrofit getRetrofitRefresh() {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        Gson gson = new GsonBuilder()
                .setLenient()
                .serializeNulls()
                .create();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(20, TimeUnit.SECONDS)
                .connectTimeout(20, TimeUnit.SECONDS);

        httpClient.addInterceptor(logging);

        Retrofit.Builder mBuilder = new Retrofit.Builder();

        if (BuildConfig.DEBUG) {
            mBuilder.client(httpClient.build());
        }

        Retrofit retrofit = mBuilder.baseUrl(AppConstants.mRefreshTokenBaseURL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(GsonConverterFactory.create()).build();

        return retrofit;
    }

}
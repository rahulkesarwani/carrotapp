package com.carrotapp.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.carrotapp.R;
import com.carrotapp.utils.customviews.CustomTextView;


/**
 * Created by innotical on 25/5/17.
 */

public class AlreadyRegisterDialog extends Dialog implements View.OnClickListener {

    private Context context;
    private String msg;

    private OkListerner okListerner;

    public AlreadyRegisterDialog(Context context, String msg) {
        super(context);
        this.context = context;
        this.msg = msg;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.dialog_invalidlogin);

        ((CustomTextView) findViewById(R.id.questionText)).setText(msg);
        findViewById(R.id.tv_ok).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_ok:
                okListerner.onYesClicked();
                break;
            default:
                break;
        }
        dismiss();
    }


    public interface OkListerner {
        public void onYesClicked();
    }

    public void setonOkListener(OkListerner listener) {
        okListerner = listener;
    }

}
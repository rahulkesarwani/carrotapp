package com.carrotapp.models.menus;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MenuRequestModel implements Serializable {

    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("hotel_id")
    @Expose
    private Integer hotelId;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("page")
    @Expose
    private Integer page;
    private final static long serialVersionUID = 5360101633632100592L;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getHotelId() {
        return hotelId;
    }

    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

}
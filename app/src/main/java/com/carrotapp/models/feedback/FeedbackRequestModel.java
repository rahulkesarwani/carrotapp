package com.carrotapp.models.feedback;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FeedbackRequestModel implements Serializable {

    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("room_id")
    @Expose
    private Integer roomId;
    @SerializedName("hotel_id")
    @Expose
    private Integer hotelId;
    @SerializedName("overall")
    @Expose
    private Integer overall;
    @SerializedName("check_in")
    @Expose
    private Integer checkIn;
    @SerializedName("bathroom")
    @Expose
    private Integer bathroom;
    @SerializedName("in_room")
    @Expose
    private Integer inRoom;
    @SerializedName("food_quality")
    @Expose
    private Integer foodQuality;
    @SerializedName("staff_friendiness")
    @Expose
    private Integer staffFriendiness;
    @SerializedName("other")
    @Expose
    private Integer other;
    @SerializedName("comment")
    @Expose
    private String comment;
    private final static long serialVersionUID = -8280649222344010166L;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Integer getOverall() {
        return overall;
    }

    public void setOverall(Integer overall) {
        this.overall = overall;
    }

    public Integer getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Integer checkIn) {
        this.checkIn = checkIn;
    }

    public Integer getBathroom() {
        return bathroom;
    }

    public void setBathroom(Integer bathroom) {
        this.bathroom = bathroom;
    }

    public Integer getInRoom() {
        return inRoom;
    }

    public void setInRoom(Integer inRoom) {
        this.inRoom = inRoom;
    }

    public Integer getFoodQuality() {
        return foodQuality;
    }

    public void setFoodQuality(Integer foodQuality) {
        this.foodQuality = foodQuality;
    }

    public Integer getStaffFriendiness() {
        return staffFriendiness;
    }

    public void setStaffFriendiness(Integer staffFriendiness) {
        this.staffFriendiness = staffFriendiness;
    }

    public Integer getOther() {
        return other;
    }

    public void setOther(Integer other) {
        this.other = other;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getHotelId() {
        return hotelId;
    }

    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }
}

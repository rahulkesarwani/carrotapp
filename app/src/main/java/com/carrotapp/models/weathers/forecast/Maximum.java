package com.carrotapp.models.weathers.forecast;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Maximum implements Serializable
{

@SerializedName("Value")
@Expose
private Double value;
@SerializedName("Unit")
@Expose
private String unit;
@SerializedName("UnitType")
@Expose
private Integer unitType;
private final static long serialVersionUID = -8792145761612192844L;

public Double getValue() {
return value;
}

public void setValue(Double value) {
this.value = value;
}

public String getUnit() {
return unit;
}

public void setUnit(String unit) {
this.unit = unit;
}

public Integer getUnitType() {
return unitType;
}

public void setUnitType(Integer unitType) {
this.unitType = unitType;
}

}
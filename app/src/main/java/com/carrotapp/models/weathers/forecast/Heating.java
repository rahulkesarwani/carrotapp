package com.carrotapp.models.weathers.forecast;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Heating implements Serializable
{

@SerializedName("Value")
@Expose
private Integer value;
@SerializedName("Unit")
@Expose
private String unit;
@SerializedName("UnitType")
@Expose
private Integer unitType;
private final static long serialVersionUID = -8366264652681263325L;

public Integer getValue() {
return value;
}

public void setValue(Integer value) {
this.value = value;
}

public String getUnit() {
return unit;
}

public void setUnit(String unit) {
this.unit = unit;
}

public Integer getUnitType() {
return unitType;
}

public void setUnitType(Integer unitType) {
this.unitType = unitType;
}

}
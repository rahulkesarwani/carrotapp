package com.carrotapp.models.weathers.current;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wind implements Serializable {

    @SerializedName("Direction")
    @Expose
    private Direction direction;
    @SerializedName("Speed")
    @Expose
    private Speed speed;
    private final static long serialVersionUID = -2037653833396070610L;

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Speed getSpeed() {
        return speed;
    }

    public void setSpeed(Speed speed) {
        this.speed = speed;
    }

}
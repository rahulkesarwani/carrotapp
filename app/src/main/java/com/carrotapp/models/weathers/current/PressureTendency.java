package com.carrotapp.models.weathers.current;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PressureTendency implements Serializable
{

@SerializedName("LocalizedText")
@Expose
private String localizedText;
@SerializedName("Code")
@Expose
private String code;
private final static long serialVersionUID = 8547133556454269647L;

public String getLocalizedText() {
return localizedText;
}

public void setLocalizedText(String localizedText) {
this.localizedText = localizedText;
}

public String getCode() {
return code;
}

public void setCode(String code) {
this.code = code;
}

}
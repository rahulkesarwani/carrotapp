package com.carrotapp.models.weathers.current;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WindGust implements Serializable
{

@SerializedName("Speed")
@Expose
private Speed speed;
private final static long serialVersionUID = 1149904667244718839L;

public Speed getSpeed() {
return speed;
}

public void setSpeed(Speed speed) {
this.speed = speed;
}

}
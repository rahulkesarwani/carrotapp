package com.carrotapp.models.weathers.current;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Past6Hours implements Serializable
{

@SerializedName("Metric")
@Expose
private Metric metric;
@SerializedName("Imperial")
@Expose
private Imperial imperial;
private final static long serialVersionUID = -8397959955264199713L;

public Metric getMetric() {
return metric;
}

public void setMetric(Metric metric) {
this.metric = metric;
}

public Imperial getImperial() {
return imperial;
}

public void setImperial(Imperial imperial) {
this.imperial = imperial;
}

}
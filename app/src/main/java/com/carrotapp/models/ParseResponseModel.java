package com.carrotapp.models;

import com.carrotapp.models.logins.LoginResponseModel;
import com.carrotapp.models.menus.CategoryModel;
import com.carrotapp.models.menus.MenuResponseModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ParseResponseModel {



    public static LoginResponseModel getLoginDetail(Object object) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(gson.toJson(object), LoginResponseModel.class);
    }

    public static ArrayList<MenuResponseModel> getMenuList(Object object) {
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<MenuResponseModel>>() {
        }.getType();
        return gson.fromJson(gson.toJson(object), listType);
    }

    public static ArrayList<CategoryModel> getCategoryList(Object object) {
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<CategoryModel>>() {
        }.getType();
        return gson.fromJson(gson.toJson(object), listType);
    }

   /* public static BankDetailModel getBankDetailData(Object object) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(gson.toJson(object), BankDetailModel.class);
    }

    public static ArrayList<TopOffer> getOffersListData(Object object) {
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<TopOffer>>() {
        }.getType();
        return gson.fromJson(gson.toJson(object), listType);
    }

    public static ArrayList<Product> getProductListData(Object object) {
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<Product>>() {
        }.getType();
        return gson.fromJson(gson.toJson(object), listType);
    }


    public static ArrayList<NotificationResponseModel> getNotificationListData(Object object) {
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<NotificationResponseModel>>() {
        }.getType();
        return gson.fromJson(gson.toJson(object), listType);
    }


    public static ProductCompareResponseModel getCompareProductData(Object object) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(gson.toJson(object), ProductCompareResponseModel.class);
    }

    public static ArrayList<CategoryModel> getCategoryListData(Object object) {
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<CategoryModel>>() {}.getType();
        return gson.fromJson(gson.toJson(object), listType);
    }
    public static CartResponseModel getCartData(Object object) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(gson.toJson(object), CartResponseModel.class);
    }

    public static ArrayList<OrderResponseModel> getOrdersListData(Object object) {
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<OrderResponseModel>>() {
        }.getType();
        return gson.fromJson(gson.toJson(object), listType);
    }


    public static ArrayList<BookMarkListModel> getFavouriteProductListData(Object object) {
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<BookMarkListModel>>() {
        }.getType();
        return gson.fromJson(gson.toJson(object), listType);
    }

    public static BookMarkResponseModel getBookMarkModel(Object object) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(gson.toJson(object), BookMarkResponseModel.class);
    }

    public static NewProductListModel getNewProductData(Object object) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(gson.toJson(object), NewProductListModel.class);
    }


    public static Product getOrderDetail(Object object) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(gson.toJson(object), Product.class);
    }

    public static OfferListModel getOfferListData(Object object) {
        if(object!=null) {
            Gson gson = new GsonBuilder().create();
            return gson.fromJson(gson.toJson(object), OfferListModel.class);
        }else {
            return new OfferListModel();
        }
    }



    public static ArrayList<OfferDetailModel> getOfferDetailData(Object object) {
        Gson gson = new GsonBuilder().create();
        Type listType = new TypeToken<ArrayList<OfferDetailModel>>() {
        }.getType();
        return gson.fromJson(gson.toJson(object), listType);
    }*/
}

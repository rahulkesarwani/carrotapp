package com.carrotapp.models.logins;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Room implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("hotel_id")
    @Expose
    private String hotelId;
    @SerializedName("room_no")
    @Expose
    private String roomNo;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    private final static long serialVersionUID = 4859874809779893557L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
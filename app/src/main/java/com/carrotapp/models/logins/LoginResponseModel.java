package com.carrotapp.models.logins;

/**
 * Created by rahul on 12/3/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponseModel implements Serializable
{

    @SerializedName("hotel")
    @Expose
    private Hotel hotel;
    @SerializedName("room")
    @Expose
    private Room room;
    private final static long serialVersionUID = 1119584692080102283L;

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

}
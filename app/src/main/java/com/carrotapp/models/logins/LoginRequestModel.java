package com.carrotapp.models.logins;

/**
 * Created by rahul on 12/3/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginRequestModel implements Serializable {

    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("room_no")
    @Expose
    private String roomNo;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("hotel_id")
    @Expose
    private Integer hotelId;
    private final static long serialVersionUID = 6175470455708334597L;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getHotelId() {
        return hotelId;
    }

    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

}
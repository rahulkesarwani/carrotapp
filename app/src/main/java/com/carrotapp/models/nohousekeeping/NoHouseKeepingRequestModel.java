package com.carrotapp.models.nohousekeeping;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NoHouseKeepingRequestModel implements Serializable {

    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("room_id")
    @Expose
    private String roomId;
    @SerializedName("email")
    @Expose
    private String email;
    private final static long serialVersionUID = 954129782812889406L;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
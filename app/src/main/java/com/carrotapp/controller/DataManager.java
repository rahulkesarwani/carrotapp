package com.carrotapp.controller;

import android.content.Context;


import com.carrotapp.apis.ApiInterface;
import com.carrotapp.controller.api.IApiHelper;
import com.carrotapp.controller.pref.IPreferenceHelper;
import com.carrotapp.di.annotations.ApplicationContext;
import com.carrotapp.utils.MySharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.disposables.CompositeDisposable;


/**
 * Created by ilkay on 11/03/2017.
 */

@Singleton
public class DataManager implements IDataManager {

    private final Context mContext;
    private final IPreferenceHelper mIPreferenceHelper;
    private final IApiHelper mIApiHelper;

    @Inject
    public DataManager(@ApplicationContext Context mContext, IPreferenceHelper mIPreferenceHelper, IApiHelper mIApiHelper) {
        this.mContext = mContext;
        this.mIPreferenceHelper = mIPreferenceHelper;
        this.mIApiHelper = mIApiHelper;
    }

    @Override
    public boolean getDatabaseCreatedStatus() {
        return mIPreferenceHelper.getDatabaseCreatedStatus();
    }

    @Override
    public void setDatabaseCreatedStatus() {
        mIPreferenceHelper.setDatabaseCreatedStatus();
    }

    @Override
    public CompositeDisposable getCompositeDisposable() {
        return mIApiHelper.getCompositeDisposable();
    }

    @Override
    public ApiInterface getApiInterface() {
        return mIApiHelper.getApiInterface();
    }

    @Override
    public boolean isInternetConnected() {
        return mIApiHelper.isInternetConnected();
    }

    @Override
    public MySharedPreferences getMySharedPreferences() {
        return null;
    }
}
